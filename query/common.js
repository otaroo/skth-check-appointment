const connection = require('../db/db.js')
const db_his = connection.his
const db_rcm = connection.rcm

function check_db() {
    db_rcm.raw(`select 'RCM database connected' as result`).then( row => {
        console.log(row[0].result);        
    })
    .catch(err => {
        console.log(err);
    });
    db_his.raw(`select 'HIS database connected' as result`).then( row => {
        console.log(row.rows[0].result);        
    })
    .catch(err => {
        console.log(err);
    });
}

let fnExport = {
    check_db :check_db
}

module.exports = fnExport