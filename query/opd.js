const connection = require("../db/db.js");
const db = connection.his;

function getOpdAllVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT vstdate,count(*)
        FROM ovst WHERE vstdate between '${startDate}' and '${ednDate}'
        GROUP BY vstdate
        ORDER BY vstdate
        `);
        resolve(row.rows);
    });
}

function getOpdSKTHVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT vstdate,count(*)
        FROM ovst WHERE vstdate between '${startDate}' and '${ednDate}' 
        and main_dep not in ('199','200','201','202','203','204','205','206','207','208','209','210','211','212','213','217')
        GROUP BY vstdate
        ORDER BY vstdate
        `);
        resolve(row.rows);
    });
}

function getOpdPuiVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT sign_date,sum(an) as count from (
            SELECT
                sign_date,
            case when depcode = '214' then count(distinct vn)  else 0 end as an
            FROM
                ovst_doctor_sign 
            WHERE
             sign_date BETWEEN '${startDate}' and '${ednDate}'
            GROUP BY
                sign_date,depcode) s1
                GROUP BY sign_date
        `);
        resolve(row.rows);
    });
}

function getOpdPuiRoomVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT sign_date,sum(an) as count from (
            SELECT
                sign_date,
            case when depcode = '215' then count(distinct vn)  else 0 end as an
            FROM
                ovst_doctor_sign 
            WHERE
             sign_date BETWEEN '${startDate}' and '${ednDate}'
            GROUP BY
                sign_date,depcode) s1
                GROUP BY sign_date
        `);
        resolve(row.rows);
    });
}

function getOpdscreen_226_Visit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT sign_date,sum(an) as count from (
            SELECT
                sign_date,
            case when depcode = '215' then count(distinct vn)  else 0 end as an
            FROM
                ovst_doctor_sign 
            WHERE
             sign_date BETWEEN '${startDate}' and '${ednDate}'
            GROUP BY
                sign_date,depcode) s1
                GROUP BY sign_date
        `);
        resolve(row.rows);
    });
}

function getOpdscreen_248_Visit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
      SELECT sign_date,sum(an) as count from (
          SELECT
              sign_date,
          case when depcode = '245' then count(distinct vn)  else 0 end as an
          FROM
              ovst_doctor_sign 
          WHERE
           sign_date BETWEEN '${startDate}' and '${ednDate}'
          GROUP BY
              sign_date,depcode) s1
              GROUP BY sign_date
      `);
        resolve(row.rows);
    });
}

function getOpdVaccineVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT day as date, 
        coalesce((SELECT count(distinct t1.vn) as count FROM ovst t1
        LEFT JOIN ovst_vaccine t2 ON t2.vn = t1.vn
        WHERE vstdate  = day
		and t2.vaccine_lot_no is not null
		and t2.person_vaccine_id in ('74','73')
        GROUP BY vstdate ),0) as count
        from generate_series(timestamp '${startDate}', timestamp '${ednDate}' , interval  '1 day') as day
    `);
        resolve(row.rows);
    });
}

function getOpdTypeVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT
            t1.vstdate as date
            ,count(t1.vn) as walkin
            ,count(t2.visit_vn) as appointment
            FROM ovst t1
            LEFT JOIN oapp t2 ON t2.visit_vn = t1.vn and t2.nextdate = t1.vstdate
            WHERE 1=1
            and t1.vstdate =  '${todayDate}'
            and t1.vsttime between '07:00:00' and '17:00:00'
            GROUP BY t1.vstdate 
        `);
        resolve(row.rows);
    });
}

function getOpdHourVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT hous::time
        ,(select count(vn) from ovst WHERE EXTRACT(HOUR FROM vsttime) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' ) as vn
        ,(SELECT count(distinct t1.vn) FROM ovst_doctor_sign t1 LEFT JOIN doctor t2 ON t2.code = t1.doctor WHERE EXTRACT(HOUR FROM sign_datetime) = EXTRACT(HOUR FROM hous) 
        and sign_date = '${todayDate}' and t2.provider_type_code in ('01','06') and active = 'Y' ) as doctor
        ,(select count(vn) from  ipt WHERE EXTRACT(HOUR FROM regtime) = EXTRACT(HOUR FROM hous)   and regdate = '${todayDate}'  ) as admit
        ,(select count(vn) from  rx_stat where dispense_datetime::date = '${todayDate}' and  EXTRACT(HOUR FROM dispense_datetime) = EXTRACT(HOUR FROM hous) ) as dispense
        ,(select count(vn) from rcpt_print where bill_date_time::date = '${todayDate}' and  EXTRACT(HOUR FROM bill_date_time) = EXTRACT(HOUR FROM hous) ) as rcpt
        from generate_series(timestamp '${todayDate} 06:00:00', timestamp '${todayDate} 17:00:00' , interval  '1 hours') as hous
        `);
        resolve(row.rows);
    });
}

function getOpdTypeHourVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT hous::time,
        (SELECT count(vn) FROM ovst where EXTRACT(HOUR FROM vsttime) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}'  ) as vn ,
        (SELECT count(vn) FROM ovst where EXTRACT(HOUR FROM vsttime) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and vn = any (select visit_vn from oapp WHERE nextdate = '${todayDate}') ) as oapp
        FROM  generate_series(timestamp '${todayDate} 07:00:00', timestamp '${todayDate} 17:00:00' , interval  '1 hours') as hous
        `);
        resolve(row.rows);
    });
}

function getOpdMedHourVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT hous::time
        ,(SELECT count(distinct t1.vn) FROM ovst_doctor_sign t1 LEFT JOIN doctor t2 ON t2.code = t1.doctor WHERE EXTRACT(HOUR FROM sign_datetime) = EXTRACT(HOUR FROM hous) 
        and sign_date = '${todayDate}'  and depcode in('120','169')
        and t2.provider_type_code = '03'
        and t1.vn =any (select vn from ovst WHERE vstdate  = '${todayDate}')
        and t1.vn =any (select vn from ovst_doctor_sign WHERE sign_date  = '${todayDate}'  and depcode in('149','148','147','146') )
       ) as vn
        ,(SELECT count(distinct t1.vn) FROM ovst_doctor_sign t1 LEFT JOIN doctor t2 ON t2.code = t1.doctor WHERE EXTRACT(HOUR FROM sign_datetime) = EXTRACT(HOUR FROM hous) 
        and sign_date = '${todayDate}' and depcode in('149','148','147','146')   and active = 'Y' ) as doctor
        from generate_series(timestamp  '${todayDate} 07:00:00', timestamp '${todayDate} 17:00:00'  , interval  '1 hours') as hous
        `);
        resolve(row.rows);
    });
}

function getTop5Diag(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
    SELECT * from (
      SELECT  rank() over ( ORDER BY count(*) desc ) 
      ,t3.name,count(*) FROM ovst t1
      LEFT JOIN ovstdiag t2 ON t2.vn = t1.vn
      LEFT JOIN icd101 t3 ON t3.code = t2.icd10
      WHERE 1=1
      and t2.diagtype = '1'
      and t1.vstdate = '${todayDate}' 
      GROUP BY t3.name
      ORDER BY count(*) desc ) s1
      WHERE s1.rank <=5
        `);
        resolve(row.rows);
    });
}

function getReferVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
    SELECT 'in' as type,count(*) FROM referin t1
    WHERE refer_date = '${todayDate}' 
    union all
    SELECT 'out' as type,count(*)  FROM referout t1
    WHERE refer_date = '${todayDate}' 
        `);
        resolve(row.rows);
    });
}


function getReferInVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
    SELECT sum(ipt) as ipt
    ,sum(ovst) as ovst from (
    SELECT (select count(vn) from ipt ip WHERE ip.vn = t1.vn ) as ipt
    ,(select count(vn) from ovst ov WHERE ov.vn = t1.vn and an is null) as ovst
    FROM referin t1
    WHERE refer_date = '${todayDate}') s1`);
        resolve(row.rows);
    });
}


function getReferOutVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
    SELECT sum(ipt) as ipt
    ,sum(ovst) as ovst from (
    SELECT (select count(vn) from ipt ip WHERE ip.vn = t1.vn ) as ipt
    ,(select count(vn) from ovst ov WHERE ov.vn = t1.vn and an is null) as ovst
    FROM referout t1
    WHERE refer_date = '${todayDate}') s1`);
        resolve(row.rows);
    });
}

function getERHourVisit(todayDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
      SELECT hous
      ,(SELECT count(*) from er_regist t1 where  t1.enter_er_time::date = '${todayDate}' and EXTRACT(HOUR FROM  t1.enter_er_time::time) = EXTRACT(HOUR FROM hous)
      and EXISTS (select 1 from er_nursing_detail nd WHERE nd.vn = t1.vn )) as trauma
      ,(SELECT count(*) from er_regist t1 where  t1.enter_er_time::date = '${todayDate}' and EXTRACT(HOUR FROM  t1.enter_er_time::time) = EXTRACT(HOUR FROM hous)
      and not EXISTS (select 1 from er_nursing_detail nd WHERE nd.vn = t1.vn )) as nontrauma
      FROM generate_series(timestamp '${todayDate} 07:00:00', timestamp '${todayDate} 17:00:00' , interval  '1 hours') hous
        `);
        resolve(row.rows);
    });
}

function getOpdVisitByDep(startDate, endDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
      SELECT gdate::date
      ,(select count(distinct t1.vn) from ovst t1
    LEFT JOIN ovst_doctor_sign t2 ON t2.vn = t1.vn
    WHERE vstdate = gdate ) as t_all
    ,(select count(distinct t1.vn) from ovst t1
    LEFT JOIN ovst_doctor_sign t2 ON t2.vn = t1.vn
    WHERE vstdate = gdate and t2.depcode = '022' ) as t079
    ,(select count(distinct t1.vn) from ovst t1
    LEFT JOIN ovst_doctor_sign t2 ON t2.vn = t1.vn
    WHERE vstdate = gdate and t2.depcode = '145' ) as t150
    ,(select count(distinct t1.vn) from ovst t1
    LEFT JOIN ovst_doctor_sign t2 ON t2.vn = t1.vn
    WHERE vstdate = gdate and t2.depcode in ('014','225','224') ) as t_pcu
    ,(select count(distinct t1.vn) from ovst t1
    WHERE vstdate = gdate and t1.ovstost in ('20','25') ) as t_shop
    from generate_series(timestamp '${endDate}', timestamp '${startDate}' , interval  '1 day') as gdate
        `);
        resolve(row.rows);
    });
}
var Model = {
    getOpdAllVisit,
    getOpdSKTHVisit,
    getOpdPuiVisit,
    getOpdPuiRoomVisit,
    getOpdTypeVisit,
    getOpdHourVisit,
    getOpdTypeHourVisit,
    getOpdMedHourVisit,
    getOpdVisitByDep,
    getTop5Diag,
    getReferVisit,
    getReferInVisit,
    getReferOutVisit,
    getERHourVisit,
    getOpdscreen_226_Visit,
    getOpdscreen_248_Visit,
    getOpdVaccineVisit

};

module.exports = Model;