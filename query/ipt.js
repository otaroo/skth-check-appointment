const connection = require("../db/db.js");
const db = connection.his;

function getIptAllWard(date) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        with ipt_covit as (
            SELECT
            ip.ward    
            ,ip.an,ip.spclty,ip.ipt_admit_type_id
            FROM
             ipt ip 
            WHERE
                1=1
                AND ip.ward IN ('21','18','02','17','16','03','24','06','01','19','07','11','10','12','04','05','23','13','14','15','20','22','25' ,'26') 
                AND ip.confirm_discharge = 'N'  
            ORDER BY ip.ward
            )

        SELECT 
        t2.name as ward
        ,(SELECT 
            COALESCE(sum(CASE 
                WHEN ward  in  ('07','19')    THEN 1
                WHEN ward not in  ('07','19')   and ip.spclty != '31'  THEN 1
                   ELSE 0 END),0)
               FROM  ipt_covit ip where ip.ward = t2.ward
        ) as an
        ,(
            CASE 
                WHEN ward = '01' THEN'32'
                WHEN ward = '02' THEN'32'
                WHEN ward = '03' THEN'32'
                WHEN ward = '04' THEN'30'
                WHEN ward = '05' THEN'32'
                WHEN ward = '06' THEN'29'
                WHEN ward = '07' THEN'27'
                WHEN ward = '10' THEN'24'
                WHEN ward = '11' THEN'13'
                WHEN ward = '12' THEN'13'
                WHEN ward = '13' THEN'17'
                WHEN ward = '14' THEN'13'
                WHEN ward = '15' THEN'13'
                WHEN ward = '16' THEN'23'
                WHEN ward = '23' THEN'10'
                WHEN ward = '22' THEN'18'
                WHEN ward = '21' THEN'8'
                WHEN ward = '19' THEN'14'
                WHEN ward = '24' THEN'20'
                WHEN ward = '25' THEN'76'
                WHEN ward = '26' THEN'16'
                ELSE
                    '0'
            END) as bed
        FROM ward t2
        WHERE 1=1
        and t2.ward not in ('20','17','18')
        ORDER BY t2.name desc`);
        resolve(row.rows);
    });
}

function getIptAnAllWard(date) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        with ipt_covit as (
            SELECT
            ip.ward   
            ,ip.an,ip.spclty,ip.ipt_admit_type_id
            FROM
              ipt ip 
            WHERE
                1=1
                AND ip.ward IN ('21','18','02','17','16','03','24','06','01','19','07','11','10','12','04','05','23','13','14','15','20','22','25','26' ) 
                AND ip.confirm_discharge = 'N'  
            ORDER BY ip.ward
            )
            SELECT 
                    t2.name as ward
                    ,(SELECT 
                    COALESCE(sum(CASE 
                        WHEN ward  in  ('07','19')    THEN 1
                        WHEN ward not in  ('07','19')   and ico.spclty != '31'  THEN 1
                            ELSE 0 END),0)
                    FROM ipt_covit ico 
                    WHERE ico.ward = t2.ward    ) as an
                    ,(
                        CASE 
                            WHEN ward = '01' THEN'32'
                            WHEN ward = '02' THEN'32'
                            WHEN ward = '03' THEN'32'
                            WHEN ward = '04' THEN'30'
                            WHEN ward = '05' THEN'32'
                            WHEN ward = '06' THEN'29'
                            WHEN ward = '07' THEN'27'
                            WHEN ward = '10' THEN'24'
                            WHEN ward = '11' THEN'13'
                            WHEN ward = '12' THEN'13'
                            WHEN ward = '13' THEN'17'
                            WHEN ward = '14' THEN'13'
                            WHEN ward = '15' THEN'13'
                            WHEN ward = '16' THEN'23'
                            WHEN ward = '23' THEN'10'
                            WHEN ward = '22' THEN'18'
                            WHEN ward = '21' THEN'8'
                            WHEN ward = '19' THEN'14'
                            WHEN ward = '24' THEN'20'
                            WHEN ward = '25' THEN'76'
                            WHEN ward = '26' THEN'16'
                            ELSE '0'
                        END) as bed
                    FROM ward t2
                    WHERE 1=1
                    and t2.ward not in ('20','17','18')
                    ORDER BY t2.name desc`);
        resolve(row.rows);
    });
}

function getIptAllType() {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT 
        coalesce(tb1.ipd_nurse_note_type_id,0) as type,
        coalesce(innt.ipd_nurse_note_type_name,'รอบันทึกประเภท') as type_name,
        sum(CASE 
            WHEN tb1.ipd_nurse_note_type_id ='1' THEN 1
            WHEN tb1.ipd_nurse_note_type_id ='2' THEN 1
            WHEN tb1.ipd_nurse_note_type_id ='3' THEN 1
            WHEN tb1.ipd_nurse_note_type_id ='4' THEN 1
            WHEN tb1.ipd_nurse_note_type_id ='5' THEN 1
            ELSE 1 END ) as an
        from 
        (SELECT ip.an,
        (SELECT ipd_nurse_note_type_id  FROM ipd_nurse_note WHERE an =ip.an ORDER BY note_date,note_time desc limit 1) as ipd_nurse_note_type_id
        FROM ipt ip
        WHERE ip.ward IN ('21','18','02','17','16','03','24','06','01','19','07','11','10','12','04','05','23','13','14','15','20','22','25','26' ) 
        AND ip.confirm_discharge = 'N' AND ip.ipt_admit_type_id != '4'   and ip.ward not in ('20','17','18') ) tb1
        LEFT JOIN ipd_nurse_note_type innt on innt.ipd_nurse_note_type_id = tb1.ipd_nurse_note_type_id
        GROUP BY tb1.ipd_nurse_note_type_id ,innt.ipd_nurse_note_type_name
        order by coalesce(tb1.ipd_nurse_note_type_id,0)
        `);
        resolve(row.rows);
    });
}

function getIptTypeByWard() {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
      SELECT 
      tt.name as ward ,
      count(type) filter (where  type = '5') as type5,
	    count(type) filter (where  type = '4') as type4,
	    count(type) filter (where  type = '3') as type3,
      count(type) filter (where  type = '2') as type2,
	    count(type) filter (where  type = '1') as type1,
	    sum(CASE WHEN type is null THEN 1 ELSE 0 END)  as untype
      from
      (
        SELECT ip.an,
      w.name,
            (SELECT  ipd_nurse_note_type_id   FROM ipd_nurse_note WHERE an =ip.an ORDER BY note_date,note_time desc limit 1) as type
            FROM ipt ip
            LEFT JOIN ward w ON w.ward = ip.ward
            WHERE ip.ward IN ('21','18','02','17','16','03','24','06','01','19','07','11','10','12','04','05','23','13','14','15','20','22','25','26' ) 
            AND ip.confirm_discharge = 'N' AND ip.ipt_admit_type_id != '4'   
            and ip.ward not in ('20','17','18') 
      ) tt
      GROUP BY tt.name
        `);
        resolve(row.rows);
    });
}

function getIptAllVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
        SELECT adm_date,count(distinct ip.an)
        FROM ipt_bed_stat ib 
        LEFT JOIN ipt ip ON ip.an = ib.an
        WHERE adm_date BETWEEN '${startDate}' and '${ednDate}' AND ip.ipt_admit_type_id != '4' 
        GROUP BY adm_date
        ORDER BY adm_date
        `);
        resolve(row.rows);
    });
}

function getIptPuiVisit(startDate, ednDate) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`       
        SELECT s1.adm_date,sum(skt) as skt ,sum(temp) as temp ,sum(_all) as _all from 
        (
        SELECT adm_date,
        case when  count(distinct ip.an) > 0 and ward != '25' then count(distinct ip.an) else 0 end as skt,
        case when  count(distinct ip.an) > 0 and ward = '25' then count(distinct ip.an) else 0 end as temp,
        case when  count(distinct ip.an) > 0 then count(distinct ip.an) else 0 end as _all
        FROM ipt_bed_stat ib 
        LEFT JOIN ipt ip ON ip.an = ib.an
        LEFT JOIN bedno bn ON bn.bedno = ib.bedno
        WHERE adm_date BETWEEN '${startDate}' and '${ednDate}' AND ip.ipt_admit_type_id != '4'  
        and bn.bedtype = '2' 
        and bed_status_type_id = '1'
        GROUP BY adm_date,ward
        ORDER BY adm_date
        ) s1
        GROUP BY s1.adm_date
        ORDER BY adm_date
        `);
        resolve(row.rows);
    });
}

var Model = {
    getIptAllWard,
    getIptAllType,
    getIptAllVisit,
    getIptPuiVisit,
    getIptAnAllWard,
    getIptTypeByWard
};

module.exports = Model;