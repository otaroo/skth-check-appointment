const connection = require("../db/db.js");
const db = connection.his;

function getDoctorActice(startDate, endDate) {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`
            with doctor as (
                SELECT dt.code FROM doctor dt
                WHERE position_id in ('1','2','31','32','60','61','62','68') and active = 'Y'
            )
            select 
            ceil(avg(s1.ti1)) as time1
            ,ceil(avg(s1.ti2)) as time2
            ,ceil(avg(s1.ti3)) as time3
            ,ceil(avg(s1.ti4)) as time4
            ,ceil(avg(s1.ti5)) as time5
            ,ceil(avg(s1.ti6)) as time6
            from (
                select 
                (
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between  '00:01:00' and '08:29:59' and t1.sign_date  = o.vstdate ) as ti1 FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                ,(
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between  '08:30:00' and '11:59:59' and t1.sign_date  = o.vstdate ) as ti2 FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                ,(
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between  '12:00:00' and '12:59:59' and t1.sign_date  = o.vstdate ) as ti3 FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                ,(
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between  '13:00:00' and '16:29:59' and t1.sign_date  = o.vstdate ) as ti4 FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                ,(
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between  '16:30:00' and '20:29:59' and t1.sign_date  = o.vstdate ) as ti5 FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                ,(
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between  '20:30:00' and '23:59:59' and t1.sign_date  = o.vstdate ) as ti6 FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                ,(
                    SELECT count(distinct doctor) filter (WHERE t1.sign_datetime::time between '00:01:00' and '23:59:59' and t1.sign_date  = o.vstdate ) as tiAll FROM ovst_doctor_sign t1
                    WHERE t1.doctor in (SELECT code FROM doctor)
                    and t1.sign_date  = o.vstdate 
                )
                from ovst o 
                where o.vstdate between '${startDate}' and '${endDate}'
                group by o.vstdate
            ) s1
        `);
      resolve(row.rows);
    } catch (err) {
      reject(err);
    }
  });
}

var Model = {
  getDoctorActice,
};

module.exports = Model;
