const connection = require('../db/db.js')
const db_his = connection.his

function getDebtByStatement(statement_id) {
    return new Promise(async (resolve, reject) => {
        let row = db_his.select('ovst.hn'
            , 'ovst_sks_billtran.invno'
            , 'opitemrece.sum_price'
            , 'ovst.vstdate'
            , 'ovst.vsttime'
            , 'patient.cid'
            , 'ovst.vn'
            , 'ovst_sks_billtran.sks_accept_no'
            , 'patient.pname'
            , 'patient.fname'
            , 'patient.lname')
            .from('ovst')
            .leftJoin('opitemrece', 'ovst.vn', 'opitemrece.vn')
            .leftJoin('patient', 'ovst.hn', 'patient.hn')
            .leftJoin('ovst_sks_billtran', 'ovst.vn', 'ovst_sks_billtran.vn')
            .where({ 'opitemrece.paidst': '02' })
            .whereExists(db_his.select('*').from('sks_statement_detail')
                .whereRaw('ovst.vstdate=sks_statement_detail.date')
                .where({ 'statement_id': statement_id }))
            .whereNotExists(db_his.select('*').from('sks_debt').whereRaw('ovst.vn=sks_debt.vn'))
            .whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
            .andWhere('opitemrece.sum_price', '>', 0)
            .whereNotNull('ovst_sks_billtran.invno')
            .orderBy('ovst.vstdate', 'asc')
            .orderBy('ovst.vsttime', 'asc')
            .catch(err => {
                console.log(err);
                reject(err)
            });
        resolve(row);
    });
}

function getStatement() {
    return new Promise((resolve, reject) => {
        var statement_no = db_his.ref('sks_statement.statement_no');
        var debt = db_his('sks_statement_detail').count('*')
            .whereExists(db_his.select('*').from('sks_debt').whereRaw('sks_statement_detail.statement_detail_id=sks_debt.statement_detail_id'))
            .where('statement_no', statement_no).as('debt');

        let row = db_his.select('*', debt)
            .from('sks_statement')
            .orderBy('statement_no', 'desc')
            .catch(err => {
                console.log(err);
                reject(err)
            });
        resolve(row);
    });
}

function getStatementDetail(statement_id) {
    return new Promise((resolve, reject) => {
        let row = db_his.select('*')
            .from('sks_statement_detail')
            .where({ 'statement_id': statement_id })
            .whereNotExists(db_his.select('*').from('sks_debt').whereRaw('sks_statement_detail.statement_detail_id=sks_debt.statement_detail_id'))
            .catch(err => {
                console.log(err);
                reject(err)
            });
        resolve(row);
    });
 }
// function getStatementList(statement_no) {
//     return new Promise((resolve, reject) => {
//         let row = db_his.select('sks_statement.statement_no', 'sks_statement_detail.hn', 'sks_statement_detail.invno', 'sks_statement_detail.date', 'sks_statement_detail.time', 'sks_statement_detail.rid', 'sks_statement_detail.amount', 'sks_debt.debt_id')
//             .from('sks_statement')
//             .leftJoin('sks_statement_detail', 'sks_statement.statement_no', 'sks_statement_detail.statement_no')
//             .leftJoin('sks_debt', 'sks_debt.statement_detail_id', 'sks_statement_detail.statement_detail_id')
//             .where({ 'sks_statement.statement_no': statement_no })
//             .catch(err => {
//                 console.log(err);
//                 reject(err)
//             });
//         resolve(row);
//     });
// }
// function getStatementCount(statement_no) {
//     return new Promise((resolve, reject) => {
//         let row = db_his.count('statement_detail_id')
//             .from('sks_statement')
//             .leftJoin('sks_statement_detail', 'sks_statement.statement_no', 'sks_statement_detail.statement_no')
//             .whereExists(db_his.select('*').from('sks_debt').whereRaw('sks_statement_detail.statement_detail_id=sks_debt.statement_detail_id'))
//             // .whereExists('sks_debt', 'sks_debt.statement_detail_id', 'sks_statement_detail.statement_detail_id')
//             .where({ 'sks_statement.statement_no': statement_no })
//             .catch(err => {
//                 console.log(err);
//                 reject(err)
//             });
//         resolve(row);
//     });
// }
function getDebByHN(hn, datestart, dateend, noinvno, nostatement, pttype) {
    return new Promise(async (resolve, reject) => {
        let row = await db_his
            .select('ovst.vn', 'ovst.hn', 'sum_price', 'ovst.vstdate', 'ovst.vsttime', 'pname', 'fname', 'lname', 'debt_id', 'statement_no', 'sks_accept_no')
            .from('ovst')
            .leftJoin('opitemrece', 'ovst.vn', 'opitemrece.vn')
            .leftJoin('patient', 'ovst.hn', 'patient.hn')
            .leftJoin('sks_debt', 'sks_debt.vn', 'ovst.vn')
            .leftJoin('ovst_sks_billtran', 'ovst.vn', 'ovst_sks_billtran.vn')
            .where({ 'opitemrece.paidst': '02', })
            .where('opitemrece.sum_price', '>', 0)
            .whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
            .where(function (w) {
                if (hn) { w.where('ovst.hn', 'like', `%${hn}%`) }
                if (datestart && dateend) { w.whereBetween('ovst.vstdate', [datestart, dateend]) }
                if (noinvno) { w.whereNull('ovst_sks_billtran.sks_accept_no').whereNull('sks_debt.debt_id')}
                if (nostatement) { w.whereNull('sks_debt.debt_id') }
                if (pttype) {
                    w.where('opitemrece.pttype', `${pttype}`)
                } else {
                    w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                }
            })
            .orderBy('ovst.vstdate', 'asc')
            .catch(err => {
                console.log(err);
                reject(err)
            });
        resolve(row);
    })
}


function getDebtCount(datestart, dateend, pttype) {
    return new Promise(async (resolve, reject) => {
        let all = await db_his.count('vn')
            .from('ovst')
            .whereBetween('ovst.vstdate', [datestart, dateend])
            .whereExists(db_his.select('*').from('opitemrece')
                .where({ 'opitemrece.paidst': '02', })
                .where('opitemrece.sum_price', '>', 0)
                .whereNull('opitemrece.an')
                .where(function (w) {
                    if (pttype) {
                        w.where('opitemrece.pttype', `${pttype}`)
                    } else {
                        w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                    }
                })
                .whereRaw('ovst.vn=opitemrece.vn'))


        let all_money = await db_his.sum('sum_price').from('opitemrece')
            .whereBetween('opitemrece.vstdate', [datestart, dateend])
            .where({ 'opitemrece.paidst': '02', })
            .where('opitemrece.sum_price', '>', 0)
            .whereNull('opitemrece.an')
            .where(function (w) {
                if (pttype) {
                    w.where('opitemrece.pttype', `${pttype}`)
                } else {
                    w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                }
            })


        let debt = await db_his.count('vn')
            .from('ovst')
            .whereBetween('ovst.vstdate', [datestart, dateend])
            .whereExists(db_his.select('*').from('sks_debt').whereRaw('ovst.vn=sks_debt.vn'))
            // .whereExists(db_his.select('*').from('ovst_sks_billtran').whereNotNull('ovst_sks_billtran.sks_accept_no').whereRaw('ovst.vn=ovst_sks_billtran.vn'))
            .whereExists(db_his.select('*').from('opitemrece')
                .where({ 'opitemrece.paidst': '02', })
                .where('opitemrece.sum_price', '>', 0)
                .whereNull('opitemrece.an')
                .where(function (w) {
                    if (pttype) {
                        w.where('opitemrece.pttype', `${pttype}`)
                    } else {
                        w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                    }
                })
                .whereRaw('ovst.vn=opitemrece.vn'));

        let debt_money = await db_his.sum('sum_price')
            .from('opitemrece')
            .whereExists(db_his.select('*').from('sks_debt').whereRaw('opitemrece.vn=sks_debt.vn'))
            .whereBetween('opitemrece.vstdate', [datestart, dateend])
            .where({ 'opitemrece.paidst': '02', })
            .where('opitemrece.sum_price', '>', 0)
            .whereNull('opitemrece.an')
            .where(function (w) {
                if (pttype) {
                    w.where('opitemrece.pttype', `${pttype}`)
                } else {
                    w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                }
            })

        let notDebt = await db_his.count('vn')
            .from('ovst')
            .whereBetween('ovst.vstdate', [datestart, dateend])
            .whereNotExists(db_his.select('*').from('sks_debt').whereRaw('ovst.vn=sks_debt.vn'))
            // .whereExists(db_his.select('*').from('ovst_sks_billtran').whereNotNull('ovst_sks_billtran.sks_accept_no').whereRaw('ovst.vn=ovst_sks_billtran.vn'))
            .whereExists(db_his.select('*').from('opitemrece')
                .where({ 'opitemrece.paidst': '02', })
                .where('opitemrece.sum_price', '>', 0)
                .whereNull('opitemrece.an')
                .where(function (w) {
                    if (pttype) {
                        w.where('opitemrece.pttype', `${pttype}`)
                    } else {
                        w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                    }
                })
                .whereRaw('ovst.vn=opitemrece.vn'));
        let notDebt_money = await db_his.sum('sum_price')
            .from('opitemrece')
            .whereNotExists(db_his.select('*').from('sks_debt').whereRaw('opitemrece.vn=sks_debt.vn'))
            .whereBetween('opitemrece.vstdate', [datestart, dateend])
            .where({ 'opitemrece.paidst': '02', })
            .where('opitemrece.sum_price', '>', 0)
            .whereNull('opitemrece.an')
            .where(function (w) {
                if (pttype) {
                    w.where('opitemrece.pttype', `${pttype}`)
                } else {
                    w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                }
            });

        let notSend = await db_his.count('vn')
            .from('ovst')
            .whereBetween('ovst.vstdate', [datestart, dateend])
            .whereExists(db_his.select('*').from('ovst_sks_billtran').whereNull('ovst_sks_billtran.sks_accept_no').whereRaw('ovst.vn=ovst_sks_billtran.vn'))
            .whereExists(db_his.select('*').from('opitemrece')
                .where({ 'opitemrece.paidst': '02', })
                .where('opitemrece.sum_price', '>', 0)
                .whereNull('opitemrece.an')
                .where(function (w) {
                    if (pttype) {
                        w.where('opitemrece.pttype', `${pttype}`)
                    } else {
                        w.whereIn('opitemrece.pttype', ['92', '55', '77', 'A6'])
                    }
                })
                .whereRaw('ovst.vn=opitemrece.vn'))
        let data = {
            all: all[0].count,
            all_money: all_money[0].sum,
            debt: debt[0].count,
            debt_money: debt_money[0].sum,
            notDebt: notDebt[0].count,
            notDebt_money: notDebt_money[0].sum,
            notSend: notSend[0].count
        }
        resolve(data);
    })
}
function getDebtComplete(datestart, dateend, pttype) {
    return new Promise(async (resolve, reject) => {
        let row = await db_his
            .select('ovst.vn', 'ovst.hn', 'ovst.vstdate', 'ovst.vsttime', 'pname', 'fname', 'lname', 'sks_statement.statement_no', 'sks_statement.statement_amount', 'bill_vol', 'bill_no', 'bill_date')
            .from('ovst')
            // .leftJoin('opitemrece', 'ovst.vn', 'opitemrece.vn')
            .leftJoin('patient', 'ovst.hn', 'patient.hn')
            .leftJoin('sks_debt', 'sks_debt.vn', 'ovst.vn')
            .leftJoin('sks_statement_detail', 'sks_statement_detail.statement_detail_id', 'sks_debt.statement_detail_id')
            .leftJoin('sks_statement', 'sks_statement.statement_id', 'sks_statement_detail.statement_id')
            // .leftJoin('ovst_sks_billtran', 'ovst.vn', 'ovst_sks_billtran.vn')
            // .leftJoin('pttype', 'opitemrece.pttype', 'pttype.pttype')
            .where(function (w) {
                if (pttype) {
                    w.whereExists(db_his.select('*').from('opitemrece').whereRaw('ovst.vn=opitemrece.vn').where('opitemrece.pttype', `${pttype}`).where('opitemrece.paidst', '02'))
                } else {
                    w.whereExists(db_his.select('*').from('opitemrece').whereRaw('ovst.vn=opitemrece.vn').whereIn('opitemrece.pttype', ['92', '55', '77', 'A6']).where('opitemrece.paidst', '02'))
                }
                if (datestart && dateend) { w.whereBetween('ovst.vstdate', [datestart, dateend]) }
            })
            .whereExists(db_his.select('*').from('sks_debt').whereRaw('ovst.vn=sks_debt.vn'))
            .orderBy('ovst.vstdate', 'asc')
            .catch(err => {
                console.log(err);
                reject(err)
            });
        resolve(row);
    })
}
function insertDebt(data) {
    return new Promise(async (resolve, reject) => {
        db_his('sks_debt').returning('debt_id').insert(data).then(id => {
            resolve(id[0]);
        }).catch(err => {
            reject(err)
            console.log(err);
        });
    });
}

function insertBill(data) {
    return new Promise(async (resolve, reject) => {
        db_his('sks_statement')
            .where('statement_no', '=', data.statement)
            .update({
                bill_vol: data.vol,
                bill_no: data.no,
                bill_date: data.date,
            }).then(id => {
                resolve(id[0]);
            }).catch(err => {
                reject(err)
                console.log(err);
            });
    });
}



function insertStatement(Statement) {
    return new Promise(async (resolve, reject) => {
        db_his('sks_statement').returning('statement_id')
            .insert(Statement).then(id => {
                resolve(id[0]);
            }).catch(err => {
                reject(err)
                console.log(err);
            });
    });
}

function insertStatementDetail(statementDetail) {
    return new Promise(async (resolve, reject) => {
        db_his('sks_statement_detail')
            .insert(statementDetail).then(id => {
                resolve(true);
            }).catch(err => {
                reject(err)
                console.log(err);
            });
    });
}

function checkStatement(statement_no, statement_type) {
    return new Promise(async (resolve, reject) => {
        let row = db_his.select().from('sks_statement').where('statement_no', statement_no).andWhere('statement_type', statement_type);
        resolve(row);
    });
}

var debModel = {
    getDebtByStatement: getDebtByStatement,
    getDebByHN: getDebByHN,
    getStatement: getStatement,
    getStatementDetail: getStatementDetail,
    insertDebt: insertDebt,
    insertBill: insertBill,
    insertStatement: insertStatement,
    insertStatementDetail: insertStatementDetail,
    checkStatement: checkStatement,
    // getStatementList: getStatementList,
    getDebtComplete: getDebtComplete,
    getDebtCount: getDebtCount,
    // getStatementCount: getStatementCount
}

module.exports = debModel;