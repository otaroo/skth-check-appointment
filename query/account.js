const connection = require('../db/db.js')
const db_his = connection.his

function getDebtPerDay(dateDebt, pttype) {
  return new Promise(async (resolve, reject) => {
    let pttypesql;
    if (pttype === undefined || pttype === '') {
      pttypesql = `t2.pttype in ('92', '55', '77', 'A6')`
    } else {
      pttypesql = `t2.pttype = '${pttype}' `
    }
    let row = await db_his.raw(`select t1.vn,t1.hn,t1.vstdate as date,t1.vsttime,t3.pname,t3.fname,t3.lname,t4.debt_id as invno,t2.pttype,cast(sum(t2.sum_price) as float)   as sum
    from ovst t1
    Left Join opitemrece t2 on t2.vn = t1.vn
    Left Join patient t3 on t3.hn = t1.hn
    Left Join rcpt_debt t4 on t4.vn = t1.vn
    Where 1=1
    and t1.vstdate = '${dateDebt}'
    and t2.paidst = '02'
    and t2.sum_price > 0
    and ${pttypesql}
    and (t2.an is null or t2.an = '')
    and t4.status is null 
    and t4.amount > 0
    and t1.vn not in (select vn from sks_debt_account WHERE vstdate = '${dateDebt}')
    GROUP BY  t1.vn,t1.hn,t1.vstdate,t1.vsttime,t3.pname,t3.fname,t3.lname,t4.debt_id,t2.pttype`)
    resolve(row.rows);
  })
}

function insertDebtPerDay(Debt) {
  return new Promise(async (resolve, reject) => {
    db_his('sks_debt_account').returning('debt_account_id').insert(Debt).then(id => {
      resolve(id[0]);
    }).catch(err => {
      reject(err)
      console.log(err);
    });
  })
}

function getStatement() {
  return new Promise((resolve, reject) => {
    let row = db_his.raw(`select *
    ,(SELECT count(*) FROM sks_statement_detail sd WHERE sd.statement_detail_id = any (SELECT statement_detail_id FROM sks_debt_account_match) and sd.statement_id =  ss.statement_id) as debtCount
    from sks_statement ss
          ORDER BY create_date
      `);
    resolve(row);
  });
}

function getDebtAccountByStatement(Statement_id) {
  return new Promise((resolve, reject) => {
    let row = db_his.raw(`select * from sks_debt_account 
      WHERE vstdate in (SELECT date from sks_statement_detail WHERE statement_id = '${Statement_id}' GROUP BY date) 
      ORDER BY create_date ` );
    resolve(row);
  });
}

function getStatementDetail(Statement_id) {
  return new Promise((resolve, reject) => {
    let row = db_his.raw(`select * from sks_statement_detail t1
    Where 1=1 and statement_id = '${Statement_id}'
    and statement_detail_id not in (SELECT statement_detail_id FROM sks_debt_account_match)
    `);
    resolve(row);
  });
}

function insertDebtMatch(Debt) {
  return new Promise(async (resolve, reject) => {
    db_his('sks_debt_account_match').returning('debt_account_id').insert(Debt).then(id => {
      resolve(id[0]);
    }).catch(err => {
      reject(err)
      console.log(err);
    });
  })
}

function getStatementNotComplete(Statement_id) {
  return new Promise((resolve, reject) => {
    let row = db_his.raw(`SELECT * FROM sks_statement_detail
    WHERE statement_id = '${Statement_id}' and statement_detail_id not in (SELECT statement_detail_id FROM sks_debt_account_match)
    `);
    resolve(row);
  });
}

function getAccDebtComplete(startDate,endDate) {
  return new Promise((resolve, reject) => {
    let row = db_his.raw(`select t1.*,t2.name as pttype_name,t5.statement_no from sks_debt_account t1
    left join pttype t2 on t2.pttype = t1.pttype
    Left Join sks_debt_account_match t3 on t3.debt_account_id = t1.debt_account_id
    Left Join sks_statement_detail t4 on t4.statement_detail_id = t3.statement_detail_id
    Left Join sks_statement t5 on t5.statement_id = t4.statement_id
    Where vstdate BETWEEN '${startDate}' and '${endDate}'
    and t1.debt_account_id in (SELECT debt_account_id from sks_debt_account_match)    
    `);
    resolve(row);
  });
}

function getAccDebtNotComplete(startDate,endDate) {
  return new Promise((resolve, reject) => {
    let row = db_his.raw(`select t1.*,t2.name as pttype_name  from sks_debt_account t1
    left join pttype t2 on t2.pttype = t1.pttype
    Where vstdate BETWEEN '${startDate}' and '${endDate}'
    and debt_account_id not in (SELECT debt_account_id from sks_debt_account_match)    
    `);
    resolve(row);
  });
}

let Model = {
  getDebtPerDay: getDebtPerDay,
  insertDebtPerDay: insertDebtPerDay,
  insertDebtMatch: insertDebtMatch,
  getStatement: getStatement,
  getDebtAccountByStatement: getDebtAccountByStatement,
  getStatementDetail: getStatementDetail,
  getStatementNotComplete: getStatementNotComplete,
  getAccDebtComplete:getAccDebtComplete,
  getAccDebtNotComplete:getAccDebtNotComplete
}

module.exports = Model;