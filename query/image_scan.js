const connection = require("../db/db.js");
const db_his_image = connection.his_image;

function getScan(startDate, endDate) {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(` 
            select dayname
            ,ceil(avg(s1.ti1)) as time1
            ,ceil(avg(s1.ti2)) as time2
            ,ceil(avg(s1.ti3)) as time3
            ,ceil(avg(s1.ti4)) as time4
            ,ceil(avg(s1.ti5)) as time5
            ,ceil(avg(s1.ti6)) as time6
            from (
            select  case when t2.day_name is null then 'วันทำการ'  else 'วันหยุด' end  as dayname
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '00:01:00' and '08:29:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti1
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '08:30:00' and '11:59:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti2
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '12:00:00' and '12:59:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti3
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '13:00:00' and '16:29:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti4
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '16:30:00' and '20:29:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti5
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '20:30:00' and '23:59:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti6
            from er_regist t1 
            LEFT JOIN holiday t2 ON t2.holiday_date = t1.vstdate
            group by t1.vstdate,t2.day_name
            ) s1
            GROUP BY dayname
            `);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

let Model = {
  getScan,
};

module.exports = Model;
