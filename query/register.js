const connection = require("../db/db.js");
const db = connection.his;

function getOccupation() {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT occupation,name FROM occupation  ORDER BY name`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function getReligion() {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT religion,name FROM religion ORDER BY name`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function getChwpart() {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT chwpart,name FROM thaiaddress
    WHERE codetype = '1'
    ORDER BY name`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function getAmppart(chwpart) {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT amppart,name FROM thaiaddress
      WHERE codetype = '2'
      and chwpart = '${chwpart}'
      ORDER BY name`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function getTmppart(chwpart, amppart) {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT tmbpart,name FROM thaiaddress
      WHERE codetype = '3'
      and chwpart = '${chwpart}'
      and amppart = '${amppart}'
      ORDER BY name`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function getMarrystatus() {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT code,name FROM marrystatus ORDER BY name`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function findRegister(cid) {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await db.raw(`SELECT * FROM skt_cov_register where cid = '${cid}' and (uploaded is null or uploaded != 'Y') limit 1`);
      resolve(row.rows);
    } catch (error) {
      reject(error);
    }
  });
}

function addRegister(person) {
  return new Promise(async (resolve, reject) => {
    try {
      let cid = await db('skt_cov_register').returning('cid').insert(person);
      console.log(cid);
      resolve(cid);
    } catch (error) {
      reject(error);
    }
  });
}

var Model = {
  getOccupation,
  getReligion,
  getChwpart,
  getAmppart,
  getTmppart,
  getMarrystatus,
  findRegister,
  addRegister
};

module.exports = Model;
