const connection = require("../db/db.js");
const db = connection.his;

function getErHourVisit(todayDate) {
    return new Promise(async (resolve, reject) => {
        let row = await db.raw(`
        SELECT hous::time
        ,(select count(vn) from ovst WHERE EXTRACT(HOUR FROM vsttime) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and vn = any (select vn from er_regist where vstdate = '${todayDate}' )  ) as vn
        ,(SELECT count(distinct t1.vn) FROM ovst_doctor_sign t1 LEFT JOIN doctor t2 ON t2.code = t1.doctor WHERE EXTRACT(HOUR FROM sign_datetime) = EXTRACT(HOUR FROM hous) 
        and sign_date = '${todayDate}' and t2.provider_type_code in ('01','06') and active = 'Y' and vn = any (select vn from er_regist where vstdate = '${todayDate}') ) as doctor
        ,(select count(vn) from  ipt WHERE EXTRACT(HOUR FROM regtime) = EXTRACT(HOUR FROM hous)   and regdate = '${todayDate}' and vn = any (select vn from er_regist where vstdate = '${todayDate}')  ) as admit
        ,(select count(vn) from  rx_stat where dispense_datetime::date = '${todayDate}' and  EXTRACT(HOUR FROM dispense_datetime) = EXTRACT(HOUR FROM hous) and vn = any (select vn from er_regist where vstdate = '${todayDate}') ) as dispense
        ,(select count(vn) from rcpt_print where bill_date_time::date = '${todayDate}' and  EXTRACT(HOUR FROM bill_date_time) = EXTRACT(HOUR FROM hous) and vn = any (select vn from er_regist where vstdate = '${todayDate}') ) as rcpt
        from generate_series(timestamp '${todayDate} 06:00:00', timestamp '${todayDate} 17:00:00' , interval  '1 hours') as hous
        `)
        resolve(row.rows);
    })
}
function getErOtHourVisit(yesterdayDate,todayDate) {
    return new Promise(async (resolve, reject) => {
        let row = await db.raw(` 
        SELECT hous::time
        ,(select count(vn) from ovst WHERE EXTRACT(HOUR FROM vsttime) = EXTRACT(HOUR FROM hous)  and vstdate between '${yesterdayDate}' and '${todayDate}' and vn = any (select vn from er_regist where  vstdate between '${yesterdayDate}' and '${todayDate}'  )) as vn
        ,(SELECT count(distinct t1.vn) FROM ovst_doctor_sign t1 LEFT JOIN doctor t2 ON t2.code = t1.doctor WHERE EXTRACT(HOUR FROM sign_datetime) = EXTRACT(HOUR FROM hous) 
        and sign_date between '${yesterdayDate}' and '${todayDate}' and t2.provider_type_code in ('01','06') and active = 'Y'  and vn = any (select vn from er_regist where  vstdate between '${yesterdayDate}' and '${todayDate}')) as doctor
        ,(select count(vn) from  ipt WHERE EXTRACT(HOUR FROM regtime) = EXTRACT(HOUR FROM hous)   and regdate between '${yesterdayDate}' and '${todayDate}'   and vn = any (select vn from er_regist where  vstdate between '${yesterdayDate}' and '${todayDate}')) as admit
        ,(select count(vn) from  rx_stat where dispense_datetime::date between '${yesterdayDate}' and '${todayDate}' and  EXTRACT(HOUR FROM dispense_datetime) = EXTRACT(HOUR FROM hous)  and vn = any (select vn from er_regist where  vstdate between '${yesterdayDate}' and '${todayDate}')) as dispense
        ,(select count(vn) from rcpt_print where bill_date_time::date between '${yesterdayDate}' and '${todayDate}' and  EXTRACT(HOUR FROM bill_date_time) = EXTRACT(HOUR FROM hous)  and vn = any (select vn from er_regist where  vstdate between '${yesterdayDate}' and '${todayDate}')) as rcpt
        from generate_series(timestamp '${yesterdayDate} 18:00:00', timestamp '${todayDate} 05:00:00' , interval  '1 hours') as hous

        `)
        resolve(row.rows);
    })
}


function getErHourVisitType(todayDate) {
    return new Promise(async (resolve, reject) => {
        let row = await db.raw(`
        SELECT hous::time
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and er_emergency_type = 1 ) as type_1
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and er_emergency_type = 2 ) as type_2
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and er_emergency_type = 3 ) as type_3
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and er_emergency_type = 4 ) as type_4
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate = '${todayDate}' and er_emergency_type = 5 ) as type_5
        from generate_series(timestamp '${todayDate} 06:00:00', timestamp '${todayDate} 17:00:00' , interval  '1 hours') as hous
        `)
        resolve(row.rows);
    })
}

function getErOtHourVisitType(yesterdayDate,todayDate) {
    return new Promise(async (resolve, reject) => {
        let row = await db.raw(`
        SELECT hous::time
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate between '${yesterdayDate}' and '${todayDate}' and er_emergency_type = 1 ) as type_1
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate between '${yesterdayDate}' and '${todayDate}' and er_emergency_type = 2 ) as type_2
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate between '${yesterdayDate}' and '${todayDate}' and er_emergency_type = 3 ) as type_3
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate between '${yesterdayDate}' and '${todayDate}' and er_emergency_type = 4 ) as type_4
        ,(select count(vn) from er_regist WHERE EXTRACT(HOUR FROM enter_er_time) = EXTRACT(HOUR FROM hous)  and vstdate between '${yesterdayDate}' and '${todayDate}' and er_emergency_type = 5 ) as type_5
        from generate_series(timestamp '${yesterdayDate} 18:00:00', timestamp '${todayDate} 05:00:00' , interval  '1 hours') as hous
        `)
        resolve(row.rows);
    })
}
function getReferTimeVisit(startDate, endDate) {
    return new Promise(async (resolve, reject) => {
        try {
            let row = await db.raw(`
                select  'referin' as typename, dayname
                ,ceil(avg(s1.ti1)) as time1
                ,ceil(avg(s1.ti2)) as time2
                ,ceil(avg(s1.ti3)) as time3
                ,ceil(avg(s1.ti4)) as time4
                ,ceil(avg(s1.ti5)) as time5
                ,ceil(avg(s1.ti6)) as time6
                from (
                select  case when t2.day_name is null then 'วันทำการ'  else 'วันหยุด' end  as dayname
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between  '00:01:00' and '08:29:59' and t1.refer_date  = o.vstdate ) as ti1 FROM referin t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between  '08:30:00' and '11:59:59' and t1.refer_date  = o.vstdate ) as ti2 FROM referin t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between  '12:00:00' and '12:59:59' and t1.refer_date  = o.vstdate ) as ti3 FROM referin t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between '13:00:00' and '16:29:59' and t1.refer_date  = o.vstdate ) as ti4 FROM referin t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between '16:30:00' and '20:29:59' and t1.refer_date  = o.vstdate ) as ti5 FROM referin t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between '20:30:00' and '23:59:59' and t1.refer_date  = o.vstdate ) as ti6 FROM referin t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                
                from ovst o 
                LEFT JOIN holiday t2 ON t2.holiday_date = o.vstdate
                where o.vstdate between '${startDate}' and '${endDate}'
                group by o.vstdate,t2.day_name
                ) s1
                GROUP BY dayname
                
                union all 
                
                select 'referout' as typename,dayname
                ,ceil(avg(s1.ti1)) as time1
                ,ceil(avg(s1.ti2)) as time2
                ,ceil(avg(s1.ti3)) as time3
                ,ceil(avg(s1.ti4)) as time4
                ,ceil(avg(s1.ti5)) as time5
                ,ceil(avg(s1.ti6)) as time6
                from (
                
                select   case when t2.day_name is null then 'วันทำการ'  else 'วันหยุด' end  as dayname
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between  '00:01:00' and '08:29:59' and t1.refer_date  = o.vstdate ) as ti1 FROM referout t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between  '08:30:00' and '11:59:59' and t1.refer_date  = o.vstdate ) as ti2 FROM referout t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between  '12:00:00' and '12:59:59' and t1.refer_date  = o.vstdate ) as ti3 FROM referout t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between '13:00:00' and '16:29:59' and t1.refer_date  = o.vstdate ) as ti4 FROM referout t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between '16:30:00' and '20:29:59' and t1.refer_date  = o.vstdate ) as ti5 FROM referout t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                ,(
                SELECT count(distinct vn) filter (WHERE t1.refer_time::time between '20:30:00' and '23:59:59' and t1.refer_date  = o.vstdate ) as ti6 FROM referout t1
                WHERE t1.refer_date  = o.vstdate and refer_point = 'ER'
                )
                
                from ovst o 
                LEFT JOIN holiday t2 ON t2.holiday_date = o.vstdate
                where o.vstdate between '${startDate}' and '${endDate}'
                group by o.vstdate,t2.day_name
                ) s1
                GROUP BY dayname
        `)
            resolve(row.rows);
        } catch (error) {
            reject(error)
        }
    })
}

function getErTimeVisit(startDate, endDate) {
    return new Promise(async (resolve, reject) => {
        try {
            let row = await db.raw(` 
            select dayname
            ,ceil(avg(s1.ti1)) as time1
            ,ceil(avg(s1.ti2)) as time2
            ,ceil(avg(s1.ti3)) as time3
            ,ceil(avg(s1.ti4)) as time4
            ,ceil(avg(s1.ti5)) as time5
            ,ceil(avg(s1.ti6)) as time6
            from (
            select  case when t2.day_name is null then 'วันทำการ'  else 'วันหยุด' end  as dayname
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '00:01:00' and '08:29:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti1
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '08:30:00' and '11:59:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti2
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '12:00:00' and '12:59:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti3
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '13:00:00' and '16:29:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti4
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '16:30:00' and '20:29:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti5
            ,count(distinct t1.vn) filter (WHERE t1.enter_er_time::time between '20:30:00' and '23:59:59' and t1.enter_er_time::date  between '2019-08-01' and '2020-08-30' )  as ti6
            from er_regist t1 
            LEFT JOIN holiday t2 ON t2.holiday_date = t1.vstdate
            group by t1.vstdate,t2.day_name
            ) s1
            GROUP BY dayname
            `)
            resolve(row.rows);
        } catch (error) {
            reject(error)
        }
    })
}

var Model = {
    getErHourVisit,
    getErOtHourVisit,
    getErHourVisitType,
    getErOtHourVisitType,
    getReferTimeVisit,
    getErTimeVisit
}

module.exports = Model;