const connection = require("../db/db.js");
const db = connection.his;

function getMoneyPerDay(startDate, endDate) {
    return new Promise(async (resolve, reject) => {
      let row = await db.raw(`
          select date,sum(money) from (
            SELECT 
            t1.vstdate as date,
            t1.sum_price as money
            FROM opitemrece t1
            LEFT JOIN ovst_seq t2 ON t2.vn = t1.vn
            WHERE 1=1
            and t2.edc_approve_list_text is not null and  t2.edc_approve_list_text != ''
            and t1.vstdate between '${startDate}' and '${endDate}'
            
            union all 
            
            SELECT t1.bill_date_time::date  as date
            ,t1.bill_amount as money 
            FROM rcpt_print t1
            WHERE 1=1
            and t1.bill_date_time between '${startDate}' and '${endDate}') tt
            GROUP BY date
            ORDER BY date
          `);
      resolve(row.rows);
    });
  }

  function getMoneyPerMonth(startDate, endDate) {
    return new Promise(async (resolve, reject) => {
      let row = await db.raw(`
          select date,sum(money) from (
            SELECT 
            EXTRACT('years' from t1.vstdate) as years,
            EXTRACT('month' from t1.vstdate) as month,
            to_char(t1.vstdate, 'MM/YYYY')  as date,
            t1.sum_price as money
            FROM opitemrece t1
            LEFT JOIN ovst_seq t2 ON t2.vn = t1.vn
            WHERE 1=1
            and t2.edc_approve_list_text is not null and  t2.edc_approve_list_text != ''
            and t1.vstdate between '${startDate}' and '${endDate}'
            
            union all 
            
            SELECT
            EXTRACT('years' from t1.bill_date_time) as years,
            EXTRACT('month' from t1.bill_date_time) as month,
            to_char(t1.bill_date_time, 'MM/YYYY')  as date,
            t1.bill_amount as money 
            FROM rcpt_print t1
            WHERE 1=1
            and t1.bill_date_time between '${startDate}' and '${endDate}') tt
            GROUP BY date,years,month
            ORDER BY years,month
          `);
      resolve(row.rows);
    });
  }

  var Model = {
    getMoneyPerDay,
    getMoneyPerMonth
  };
  
  module.exports = Model;
  