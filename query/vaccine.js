const connection = require("../db/db.js");
const db = connection.his;

function getPatientVaccinePerDay(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT 
      t1.hn,
      concat(t3.pname,t3.fname,' ',t3.lname) as patient_name,
      t2.entry_datetime::date,
      t2.vaccine_plan_no,
      t2.moph_update_result
      FROM ovst t1
      INNER JOIN ovst_vaccine t2 ON t2.vn = t1.vn
      LEFT JOIN patient t3 ON t3.hn = t1.hn
      WHERE 1=1
      and t2.person_vaccine_id in ('74','73')
      and t2.immunization_datetime::date = '${Date}'
      and t2.vaccine_lot_no is not null
      and (t2.moph_update_result not like '%ผิดพลาด%' OR  t2.moph_update_result is null)
      ORDER BY t2.immunization_datetime desc
          `);
    resolve(row.rows);
  });
}

function getPatientVaccineType(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT 
      sum(CASE WHEN symptom = 'Astra Zeneca เข็มที่ 1' THEN 1 ELSE 0 END) as Astra1
      ,sum(CASE WHEN symptom = 'Astra Zeneca เข็มที่ 2' THEN 1 ELSE 0 END) as Astra2
      ,sum(CASE WHEN symptom = 'Sinovac เข็มที่ 1' THEN 1 ELSE 0 END) as Sinovac1
      ,sum(CASE WHEN symptom = 'Sinovac เข็มที่ 2' THEN 1 ELSE 0 END) as Sinovac2
      FROM ovst t1 
      left join opdscreen t2 on t2.vn = t1.vn
      WHERE main_dep = '243'
      and t2.cc is not null
      and t1.vstdate = '${Date}'
          `);
    resolve(row.rows);
  });
}

function getCountPatientVaccine() {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT 
        vaccine_plan_no,count(*)
        FROM ovst t1
        INNER JOIN ovst_vaccine t2 ON t2.vn = t1.vn
        LEFT JOIN patient t3 ON t3.hn = t1.hn
        WHERE 1=1
        and t2.person_vaccine_id in ('74','73')
        and t2.vaccine_lot_no is not null
        GROUP BY vaccine_plan_no
          `);
    resolve(row.rows);
  });
}

function getCountPatientCC(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT count(distinct t1.vn) FROM ovst t1
        LEFT JOIN opdscreen t2 ON t2.vn = t1.vn
        WHERE 1=1
        and t1.main_dep = '243'
        and t2.cc is not null
        and t1.vstdate = '${Date}'
        
          `);
    resolve(row.rows);
  });
}

function getCountVisitVaccine(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT 
        count(vn)
        FROM ovst t1
        WHERE 1=1
        and t1.main_dep = '243'
        and t1.vstdate = '${Date}'
        
          `);
    resolve(row.rows);
  });
}

function getPatientObserv(Date, page) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT
        t1.hn,
        concat(t3.pname,t3.fname,' ',t3.lname) as patient_name,
        to_char(t2.immunization_datetime::time,'HH24:MI') as starttime,
        to_char((t2.immunization_datetime + (30 * interval '1 minute'))::time,'HH24:MI') as endtime,
        Round(EXTRACT(EPOCH FROM  (NOW()-t2.immunization_datetime)::INTERVAL)/60) as min
        FROM ovst t1
        INNER JOIN ovst_vaccine t2 ON t2.vn = t1.vn
        LEFT JOIN patient t3 ON t3.hn = t1.hn
        LEFT JOIN kskdepartment t4 ON t4.depcode = t1.main_dep
        WHERE 1=1
        and t2.person_vaccine_id in ('74','73')
        and t2.vaccine_lot_no is not null
        and t1.vstdate = '${Date}'
        and Round(EXTRACT(EPOCH FROM  (NOW()-t2.immunization_datetime)::INTERVAL)/60) >= 29
        and t2.moph_update_result is null
        and t2.immunization_datetime is not null
        ORDER BY t2.immunization_datetime desc
        LIMIT 5 offset (${page}*5)-5
          `);
    resolve(row.rows);
  });
}

function getPatientObservTotal(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT
        count(distinct t1.hn)
        FROM ovst t1
        INNER JOIN ovst_vaccine t2 ON t2.vn = t1.vn
        LEFT JOIN patient t3 ON t3.hn = t1.hn
        LEFT JOIN kskdepartment t4 ON t4.depcode = t1.main_dep
        WHERE 1=1
        and t2.person_vaccine_id in ('74','73')
        and t2.vaccine_lot_no is not null
        and t1.vstdate = '${Date}'
        and Round(EXTRACT(EPOCH FROM  (NOW()-t2.immunization_datetime)::INTERVAL)/60) >= 29
        and t2.moph_update_result is null
        and t2.immunization_datetime is not null
          `);
    resolve(row.rows);
  });
}

function getPatientFail(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT
        t1.hn,
        concat(t3.pname,t3.fname,' ',t3.lname) as patient_name,
        t3.mobile_phone_number,
        t1.main_dep,
        t3.cid,
        t1.vstdate + t1.vsttime as date,
        t2.entry_datetime::date,
        t2.vaccine_plan_no,
        t2.vaccine_lot_no
        ,t2.immunization_datetime 
        ,t2.moph_update_result
        ,t2.moph_update_status
        ,Round(EXTRACT(EPOCH FROM  (NOW()-(t1.vstdate+t1.vsttime))::INTERVAL)/60) 
        ,t1.oqueue
        ,t1.staff
        ,t4.cc
				,t5.oapp_id
        ,(SELECT count(*) FROM ovst_vaccine ov where ov.vn = t1.vn ) as vaccine_count
        ,(SELECT count(*) FROM ovst ovs where ovs.hn = t1.hn and ovs.vstdate = t1.vstdate ) as visit_count
        ,(SELECT string_agg( k.department ,' | ') from ovst o left join kskdepartment k on k.depcode = o.main_dep where o.hn= t1.hn and o.vstdate = t1.vstdate ) as dep
        ,t1.vn
        FROM ovst t1
        LEFT JOIN ovst_vaccine t2 ON t2.vn = t1.vn
        LEFT JOIN patient t3 ON t3.hn = t1.hn
        LEFT JOIN opdscreen t4 ON t4.vn = t1.vn
				LEFT JOIN oapp t5 ON t5.vn = t1.vn
        WHERE 1=1
        and t1.main_dep = '243'
        and t2.vaccine_lot_no is null
        and t1.vstdate = '${Date}'
        and (CASE	WHEN NOW()::time > '15:00' 
        THEN Round(EXTRACT(EPOCH FROM  (NOW()-(t1.vstdate+t1.vsttime))::INTERVAL)/60) >= 1 
        ELSE Round(EXTRACT(EPOCH FROM  (NOW()-(t1.vstdate+t1.vsttime))::INTERVAL)/60) >= 30 END)
        ORDER BY t1.vstdate+t1.vsttime
          `);
    resolve(row.rows);
  });
}

function getVaccineByBrand() {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
        SELECT
        SUM(CASE WHEN t2.person_vaccine_id = '73' THEN 1 ELSE 0 END) as AstraZeneca,
        SUM(CASE WHEN t2.person_vaccine_id = '73' and t2.vaccine_plan_no = '1' THEN 1 ELSE 0 END) as AstraZeneca1,
        SUM(CASE WHEN t2.person_vaccine_id = '73' and t2.vaccine_plan_no = '2' THEN 1 ELSE 0 END) as AstraZeneca2,
        SUM(CASE WHEN t2.person_vaccine_id = '74' THEN 1 ELSE 0 END) as Sinovac,
        SUM(CASE WHEN t2.person_vaccine_id = '74' and t2.vaccine_plan_no = '1' THEN 1 ELSE 0 END) as Sinovac1,
        SUM(CASE WHEN t2.person_vaccine_id = '74' and t2.vaccine_plan_no = '2' THEN 1 ELSE 0 END) as Sinovac2
        FROM ovst t1
        LEFT JOIN ovst_vaccine t2 ON t2.vn = t1.vn
        WHERE 1=1
        and t1.vstdate > '2021-04-01'
          `);
    resolve(row.rows);
  });
}
function getVaccinePerDay() {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT day::date as date
    ,(SELECT count(vn) FROM ovst_vaccine WHERE person_vaccine_id = '74' and vaccine_lot_no is not null and immunization_datetime::date = day) as vaccine
    ,(SELECT count(vn) FROM ovst_vaccine WHERE person_vaccine_id = '74' and vaccine_lot_no is not null and immunization_datetime::date = day and vaccine_plan_no = '1') as plan1
    ,(SELECT count(vn) FROM ovst_vaccine WHERE person_vaccine_id = '74' and vaccine_lot_no is not null and immunization_datetime::date = day and vaccine_plan_no = '2') as plan2
    from generate_series(timestamp '2021-05-01', timestamp '2021-05-13' , interval  '1 day') as day
          `);
    resolve(row.rows);
  });
}

function getVaccineSummary() {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT 
    SUM(CASE WHEN t2.vaccine_plan_no = '1' THEN 1 ELSE 0 END) as plan1,
    SUM(CASE WHEN t2.vaccine_plan_no = '2' THEN 1 ELSE 0 END) as plan2,
    SUM(CASE WHEN t3.sex = '1' THEN 1 ELSE 0 END) as man,
    SUM(CASE WHEN t3.sex = '2' THEN 1 ELSE 0 END) as woman,
    MAX(EXTRACT(year FROM age(t1.vstdate,t3.birthday))) as อายุ_MAX,
    MIN(EXTRACT(year FROM age(t1.vstdate,t3.birthday))) as อายุ_MIN,
    ROUND( AVG(EXTRACT(year FROM age(t1.vstdate,t3.birthday)))) as อายุ_AVG

    FROM ovst t1
    LEFT JOIN ovst_vaccine t2 ON t2.vn = t1.vn
    LEFT JOIN patient t3 ON t3.hn = t1.hn
    WHERE 1=1
    and t1.main_dep = '243'
    and t2.person_vaccine_id in ('74','73')
    and t1.vstdate > '2021-04-01'
    and t2.vaccine_lot_no is not null
          `);
    resolve(row.rows);
  });
}

function getWorngDepVaccine(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT t1.hn
    ,concat(t3.pname,t3.fname,' ',t3.lname) as name
    ,t5.department
    ,t2.cc
    ,t4.vaccine_lot_no 
    ,t1.vn
    
    FROM ovst t1
    left join opdscreen t2 on t2.vn = t1.vn
    left join patient t3 on t3.hn = t1.hn
    left join ovst_vaccine t4 on t4.vn = t1.vn
    left join kskdepartment	t5 on t5.depcode = t1.main_dep
    WHERE main_dep <> '243'
    and t1.vstdate = '${Date}'
    and t4.vaccine_lot_no is not null
    and t4.person_vaccine_id in ('73','74')
    ORDER BY t1.vsttime
        `);
    resolve(row.rows);
  });
}

function getVisitFail(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT  
    t2.hn
    ,concat(t3.pname,t3.fname,' ',t3.lname) as name
    ,t3.mobile_phone_number
    ,t2.symptom
    ,t5.name as type
    ,t1.staff
    ,t1.vn

    FROM ovst t1
    left join opdscreen t2 on t2.vn = t1.vn
    left join patient t3 on t3.hn = t1.hn
    left join ovst_seq	t4 on t4.vn = t1.vn
    left join er_pt_type t5 on t5.er_pt_type = t4.er_pt_type
    WHERE main_dep ='243'
    and t1.vstdate = '${Date}'
    and (t3.mobile_phone_number is null OR t2.symptom is null OR t3.mobile_phone_number = '' OR t4.er_pt_type != '3')
    ORDER BY t1.vsttime
        `);
    resolve(row.rows);
  });
}

function getAllVisit(Date) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT  
    t2.hn
    ,concat(t3.pname,t3.fname,' ',t3.lname) as name
    ,t3.mobile_phone_number
    ,t2.symptom
    ,t5.name as type
    ,t1.staff
    ,t1.vn
    ,t2.cc

    FROM ovst t1
    left join opdscreen t2 on t2.vn = t1.vn
    left join patient t3 on t3.hn = t1.hn
    left join ovst_seq	t4 on t4.vn = t1.vn
    left join er_pt_type t5 on t5.er_pt_type = t4.er_pt_type
    WHERE main_dep ='243'
    and t1.vstdate = '${Date}'
    ORDER BY t2.symptom
        `);
    resolve(row.rows);
  });
}

function getVaccinePercent(startDate,endDate, personCount) {
  return new Promise(async (resolve, reject) => {
    let row = await db.raw(`
    SELECT to_char(day , 'YYYY-MM-DD') as date
    ,(SELECT count(vn) FROM ovst_vaccine WHERE person_vaccine_id in ('74','73') and vaccine_lot_no is not null and immunization_datetime::date <= day) as vaccine
    ,(SELECT Round(count(vn)/ Round(${personCount},2)*100,2) FROM ovst_vaccine WHERE person_vaccine_id in ('74','73') and vaccine_lot_no is not null and immunization_datetime::date <= day) as percent
        from generate_series(timestamp '${startDate}', timestamp '${endDate}' , interval  '1 day') as day
        WHERE EXTRACT(day FROM day) in ('1','16')
    `);
    resolve(row.rows);
  });
}

var Model = {
  getPatientVaccinePerDay,
  getCountPatientVaccine,
  getCountVisitVaccine,
  getPatientObserv,
  getPatientObservTotal,
  getVaccineByBrand,
  getVaccineSummary,
  getVaccinePerDay,
  getPatientFail,
  getCountPatientCC,
  getPatientVaccineType,
  getWorngDepVaccine,
  getVisitFail,
  getAllVisit,
  getVaccinePercent
};

module.exports = Model;
