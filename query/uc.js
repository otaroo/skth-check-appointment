const connection = require("../db/db.js");
const db_his = connection.his;

function insertDebtUC(Debt) {
  return new Promise(async (resolve, reject) => {
    db_his
      .select("an")
      .from("uc_ipd_debt")
      .where("an", Debt.an)
      .then(an => {
        if (an.length === 0) {
          console.log(`insert an => ${Debt.an}`);
          db_his("uc_ipd_debt")
            .returning("uc_ipd_debt_id")
            .insert(Debt)
            .then(id => {
              resolve(1);
            })
            .catch(err => {
              reject(err);
              console.log(err);
            });
        }else{
          console.log(`duplicate an => ${Debt.an}`);
          resolve(0);
        }
      })
      .catch(err => {
        reject(err);
        console.log(err);
      });

    // db_his("uc_ipd_debt")
    //   .returning("uc_ipd_debt_id")
    //   .insert(Debt)
    //   .then(id => {
    //     resolve(id[0]);
    //   })
    //   .catch(err => {
    //     reject(err);
    //     console.log(err);
    //   });
  });
}

function insertStatementUC(Statement) {
  return new Promise(async (resolve, reject) => {
    db_his
      .select("an")
      .from("uc_ipd_statement")
      .where("an", Statement.an)
      .then(an => {
        if (an.length === 0) {
          console.log(`insert an => ${Statement.an}`);
          db_his("uc_ipd_statement")
            .returning("uc_ipd_statement_id")
            .insert(Statement)
            .then(id => {
              resolve(1);
            })
            .catch(err => {
              reject(err);
              console.log(err);
            });
        }else{
          console.log(`duplicate an => ${Statement.an}`);
          resolve(0);
        }
      })
      .catch(err => {
        reject(err);
        console.log(err);
      });


  });
}

let Model = {
  insertDebtUC: insertDebtUC,
  insertStatementUC:insertStatementUC
};

module.exports = Model;
