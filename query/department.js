const connection = require("../db/db.js");
const db = connection.his;

function getOpdWait(dateToday) {
    return new Promise(async(resolve, reject) => {
        let row = await db.raw(`
            with tb_queue as (SELECT
                o2.*
                FROM
                    opd_dep_queue o1,
                    ovst o2 
                WHERE
                  1=1
                    AND o1.tx_status = 'W' 
                    AND o1.vn = o2.vn 
                    AND o2.vstdate = '${dateToday}')
                , tb_sign as (SELECT vn,depcode FROM ovst_doctor_sign  where sign_datetime::date = '${dateToday}')
                    
                SELECT
                CASE WHEN kp.depcode = '119' THEN 'เด็ก'
									WHEN kp.depcode = '169' THEN 'Med'
									WHEN kp.depcode = '120' THEN 'นัด Med'
									WHEN kp.depcode = '121' THEN 'Sur'
									WHEN kp.depcode = '162' THEN 'URO'
									WHEN kp.depcode = '157' THEN 'Ortho'
									WHEN kp.depcode = '163' THEN 'Gyne'
									WHEN kp.depcode = '031' THEN 'DM'
									WHEN kp.depcode = '019' THEN 'Poor DM'
									WHEN kp.depcode = '020' THEN 'HT'
									WHEN kp.depcode = '116' THEN 'Eye'
									WHEN kp.depcode = '117' THEN 'ENT'
									WHEN kp.depcode = '118' THEN 'Dent'
									WHEN kp.depcode = '032' THEN 'Psy'
                                    WHEN kp.depcode = '168' THEN 'กายภาพ'
                                    WHEN kp.depcode = '071' THEN 'General'
									WHEN kp.depcode = '150' THEN 'ANC'
									WHEN kp.depcode = '151' THEN 'WBC'
									WHEN kp.depcode = '220' THEN 'screening'
                                    WHEN kp.depcode = '245' THEN 'คัดกรอง'
                                    WHEN kp.depcode = '243' THEN 'ฉีดวัคซีน'
									ELSE
										 kp.department
								  END as department
                ,(SELECT count(distinct vn) FROM tb_queue tq where tq.cur_dep = kp.depcode) as wait
                ,(SELECT count(distinct vn) FROM tb_sign ts WHERE ts.depcode = kp.depcode) as sign
                FROM kskdepartment kp 
                WHERE depcode in ('119','169','120','121','162','157','163','031','019','020','116','117','118','032','168','071','150','151','220','245','243')
                
                union all				
                
                SELECT 
                'การเงิน' as department
                ,(SELECT count(distinct o1.vn) from opd_dep_queue o1 ,ovst o2  where o1.depcode='111' and o1.tx_status='W' and o1.vn = o2.vn and o2.vstdate> '${dateToday}') as wait
                ,(SELECT count(distinct r.vn) FROM rcpt_print r WHERE r.bill_date = '${dateToday}') as sign
                
                union all				
                
                SELECT 
                'รอรับยา' as department
                ,(select count(distinct vn) FROM opitemrece
                WHERE vn = any (SELECT vn FROM ovst_doctor_sign where sign_datetime::date = '${dateToday}')
                and vstdate = '${dateToday}'
                and icode = any (SELECT icode FROM drugitems)
                and vn not in (select vn from rx_stat WHERE dispense_datetime :: date = '${dateToday}' )) as wait
                ,(select count(distinct vn) FROM opitemrece
                WHERE vn = any (SELECT vn FROM ovst_doctor_sign where sign_datetime::date = '${dateToday}')
                and vstdate = '${dateToday}'
                and icode = any (SELECT icode FROM drugitems)
                and vn = any (select vn from rx_stat WHERE dispense_datetime :: date = '${dateToday}' )) as sign
               
            `)
        resolve(row.rows);
    })
}

var Model = {
    getOpdWait
}

module.exports = Model;