function arrMatch(arg1, arg2) {
  return new Promise(async (resolve, reject) => {
    let data = arg1
      .filter(function (ar1) {
        return arg2.some(function (ar2) {
          return (
            ar1.hn === ar2.hn &&
            ar1.date === ar2.date &&
            ar1.time === ar2.time &&
            ar1.sum === ar2.amount &&
            ar1.invno.indexOf(ar2.invno) > -1
          );
        });
      })
      .map(function (o) {
        let i = arg2.filter(function (e) {
          return (
            e.hn === o.hn &&
            e.date === o.date &&
            e.time === o.time &&
            e.amount === o.sum &&
            o.invno.indexOf(e.invno) > -1
          );
        });
        return {
          hisvn: `${o.vn}|${i[0].statement_detail_id}`,
          hishn: o.hn,
          hissum: o.sum,
          hisdate: o.date,
          histime: o.time,
          rid: o.rid,
          xmlhn: i[0].hn,
          xmlsum: i[0].amount,
          xmldate: i[0].date,
          xmltime: i[0].time,
        };
      });
    resolve(data);
  });
}
function accountMatch(arg1, arg2) {
  return new Promise(async (resolve, reject) => {
    let data = arg1
      .filter(function (ar1) {
        return arg2.some(function (ar2) {
          return (
            ar1.hn === ar2.hn &&
            ar1.date === ar2.date &&
            ar1.time === ar2.time &&
            ar1.sum === ar2.amount &&
            ar1.invno.indexOf(ar2.invno) > -1
          );
        });
      })
      .map(function (o) {
        let i = arg2.filter(function (e) {
          return (
            e.hn === o.hn &&
            e.date === o.date &&
            e.time === o.time &&
            e.amount === o.sum &&
            o.invno.indexOf(e.invno) > -1
          );
        });
        return {
          vn: `${o.vn}`,
          statement_detail_id: i[0].statement_detail_id,
          debt_account_id: o.debt_account_id,
        };
      });
    resolve(data);
  });
}
function invnoMatch(arg1, arg2) {
  return new Promise(async (resolve, reject) => {
    let data = arg1
      .filter(function (ar1) {
        return arg2.some(function (ar2) {
          return ar1.sum === ar2.amount && ar1.invno === ar2.invno;
        });
      })
      .map(function (o) {
        let i = arg2.filter(function (e) {
          return e.amount === o.sum && e.invno === o.invno;
        });
        return {
          hisvn: `${o.vn}|${i[0].statement_detail_id}`,
          hishn: o.hn,
          hissum: o.sum,
          hisdate: o.date,
          histime: o.time,
          rid: o.rid,
          xmlhn: i[0].hn,
          xmlsum: i[0].amount,
          xmldate: i[0].date,
          xmltime: i[0].time,
        };
      });
    resolve(data);
  });
}

function arrNotMatch(arg1, arg2) {
  return new Promise(async (resolve, reject) => {
    let data = arg1
      .filter(function (ar1) {
        return arg2.some(function (ar2) {
          return (
            ar1.hn === ar2.hn &&
            ar1.date === ar2.date &&
            ar1.time === ar2.time &&
            ar1.sum != ar2.amount &&
            ar1.invno === ar2.invno
          );
        });
      })
      .map(function (o) {
        let i = arg2.filter(function (e) {
          return (
            e.hn === o.hn &&
            e.date === o.date &&
            e.time === o.time &&
            e.amount != o.sum &&
            e.invno === o.invno
          );
        });
        return {
          hisvn: o.vn,
          hishn: o.hn,
          hissum: o.sum,
          hisdate: o.date,
          histime: o.time,
          rid: o.rid,
          xmlhn: i[0].hn,
          xmlsum: i[0].amount,
          xmldate: i[0].date,
          xmltime: i[0].time,
        };
      });
    resolve(data);
  });
}

function getMonth(thmonth) {
  let month = [
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน",
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฏาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤศจิกายน",
    "ธันวาคม",
  ];
  if (month.findIndex((f) => f === thmonth) != -1) {
    return month.findIndex((f) => f === thmonth) + 1;
  }
  let months_th_mini = [
    "ม.ค.",
    "ก.พ.",
    "มี.ค.",
    "เม.ย.",
    "พ.ค.",
    "มิ.ย.",
    "ก.ค.",
    "ส.ค.",
    "ก.ย.",
    "ต.ค.",
    "พ.ย.",
    "ธ.ค.",
  ];
  if (months_th_mini.findIndex((f) => f === thmonth) != -1) {
    return months_th_mini.findIndex((f) => f === thmonth);
  }
}
function day(thday) {
  let day = ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"];
  return day.findIndex((f) => f === thday);
}

function checkMod11(cid) {
  return new Promise(async (resolve, reject) => {
    if (cid.length != 13) resolve(false);
    for (i = 0, sum = 0; i < 12; i++) {
      sum += parseFloat(cid.charAt(i)) * (13 - i);
    }
    if ((11 - (sum % 11)) % 10 != parseFloat(cid.charAt(12))) resolve(false);
    resolve(true);
  });
}

var commonfn = {
  arrMatch: arrMatch,
  arrNotMatch: arrNotMatch,
  getMonth: getMonth,
  invnoMatch: invnoMatch,
  accountMatch: accountMatch,
  checkMod11
};
module.exports = commonfn;
