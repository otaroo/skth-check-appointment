const axios = require("axios");
const FormData = require("form-data");

function check(token) {
  return new Promise(async (resolve, reject) => {
    try {
      if (!token) reject(false);
      let secret_key = process.env.secret_key;
      let successReCaptcha;
      let errorReCaptcha;
      const formData = new FormData();
      formData.append("secret", secret_key);
      formData.append("response", token);

      await axios
        .post(`https://www.google.com/recaptcha/api/siteverify`, formData, {
          headers: formData.getHeaders(),
        })
        .then(function (response) {
          //handle success
          successReCaptcha = response.data;
          console.log("data", successReCaptcha);
        })
        .catch(function (response) {
          //handle error
          errorReCaptcha = response.data;
          console.log("error", successReCaptcha);
          reject(false);
        });

      if (!successReCaptcha.success) {
        reject(false);
      } else {
        resolve(true);
      }
    } catch (error) {
      console.log(error);
      reject(false);
    }
  });
}

module.exports = check;
