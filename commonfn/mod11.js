function check(cid) {
  return new Promise(async (resolve, reject) => {
    try {
      if (cid.length != 13) resolve(false);
      for (i = 0, sum = 0; i < 12; i++) {
        sum += parseFloat(cid.charAt(i)) * (13 - i);
      }
      if ((11 - (sum % 11)) % 10 != parseFloat(cid.charAt(12))) resolve(false);
      resolve(true);
    } catch (error) {
      console.log(error);
      reject(false);
    }
  });
}

module.exports = check;
