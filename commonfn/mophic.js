const express = require("express");
const router = express.Router();
const axios = require("axios");
const https = require("https");
const job = require("../job/token");

const agent = new https.Agent({
  rejectUnauthorized: false,
});

function ImmunizationTarget(cid) {
  return new Promise(async (resolve, reject) => {
    try {
      let token =await job.getToken();
      let appointment = await axios
        .get(`https://cvp1.moph.go.th/api/ImmunizationTarget?cid=${cid}`, {
          httpsAgent: agent,
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .catch((e) => {
          let { message } = e.toJSON();
          return { error: true, data: "error : " + message };
        });

      if (appointment.error) {
        let data = {
          error: true,
          message: "ไม่พบข้อมูลการลงทะเบียน",
        };
        reject(false);
        return;
      }
      
      resolve(appointment.data.result);
    } catch (error) {
      console.log(error);
      reject(false);
    }
  });
}
let fn = {
  ImmunizationTarget,
};

module.exports = fn;
