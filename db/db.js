require('dotenv').config()


let his = require('knex')({
    client: process.env.his_database_type,
    connection: {
        host: process.env.his_database_host,
        user: process.env.his_database_user,
        password: process.env.his_database_password,
        database: process.env.his_database_name
    }
});

let his_image = require('knex')({
    client: process.env.his_image_database_type,
    connection: {
        host: process.env.his_image_database_host,
        user: process.env.his_image_database_user,
        password: process.env.his_image_database_password,
        database: process.env.his_image_database_name
    }
});


var db = {
    his,
    his_image
}

module.exports = db;