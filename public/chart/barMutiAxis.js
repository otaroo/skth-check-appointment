function barMutiAxisChart(idChart, data) {
    let ctx = document.getElementById(idChart).getContext('2d');
    let reChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            maintainAspectRatio: false,
            plugins: {
                datalabels: {

                    color: 'black',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    },
                    FontSize: '20',
                    title: false
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontSize: 16,
                        padding: 10
                    }
                }],
                yAxes: [{
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "left",
                        id: "y-axis-1",
                    },
                    {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "right",
                        id: "y-axis-2",
                        gridLines: {
                            drawOnChartArea: false,
                        },
                    }
                ]
            },
            legend: {
                display: true,
                labels: {
                    fontSize: 16
                },
                align: 'start'
            },
            legendCallback: function(chart) {
                console.log(chart);
                return ''
            }
        }
    });
    return reChart
}