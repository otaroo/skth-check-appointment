function pieChart(idChart, data) {
    let ctypevisit = document.getElementById(idChart).getContext("2d");
    let reChart = new Chart(ctypevisit, {
        type: 'pie',
        data: data,
        tooltips: false,
        options: {
            maintainAspectRatio: false,
            plugins: {
                datalabels: {
                    label: 'Active bed',
                    color: '#000',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    },
                    // formatter: Math.round,
                    FontSize: '20',
                    title: false
                }
            },
            legend: {
                display: true,
                labels: {
                    fontSize: 16
                },
                align: 'center'
            }
        }
    });
    return reChart
}