function barChart(idChart, data) {
    let ctx = document.getElementById(idChart).getContext('2d');
    let reChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            maintainAspectRatio: false,
            plugins: {
                datalabels: {

                    color: 'black',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    },
                    FontSize: '20',
                    title: false
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontSize: 16,
                        padding: 10
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontSize: 16,
                        padding: 20
                    }
                }]
            },
            legend: {
                display: true,
                labels: {
                    fontSize: 16
                },
                align: 'start'
            },
            legendCallback: function(chart) {
                console.log(chart);
                return ''
            }
        }
    });
    return reChart
}