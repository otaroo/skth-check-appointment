function lineChart(idChart, data) {
    let ctx = document.getElementById(idChart).getContext("2d");
    let reChart = new Chart(ctx, {
        type: 'line',
        data: data,
        tooltips: false,
        options: {
            maintainAspectRatio: false,
            plugins: {
                datalabels: {
                    color: 'black',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    },
                    // formatter: Math.round,
                    // FontSize: '20',
                    title: false,
                    // align: 'top',
                    // anchor: 'end',
                    // display: 'auto',
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontSize: 16,
                        padding: 10
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontSize: 16,
                        padding: 20
                    }
                }]
            },
            legend: {
                display: 'auto',
                labels: {
                    fontSize: 16
                },
                align: 'start'
            }
        }
    });
    return reChart
}