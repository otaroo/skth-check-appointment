

export default () => {

    app.config(function ($routeProvider) {
        $routeProvider.when("/opd/test", { templateUrl: "page/opd/test.html", controller: "test", title: "test" })
    })
    app.controller('test', function ($scope, $http, $resource, $q, $interval) {

        let typevisitline
        $scope.itemSelect = moment().format('YYYY-MM-DD')
        init();
        getData($scope.itemSelect);
        function getData(date) {
            let sendData = {
                date: date
            }
            let typevisit = $http.post('/opd/hourvisit', sendData)
            $q.all([typevisit]).then(result => {
                typevisitline.data.datasets[1].data = result[0].data.map(data => data.doctor)
                typevisitline.data.datasets[0].data = result[0].data.map(data => data.dispense)
                typevisitline.data.labels = result[0].data.map(data => data.hous.substr(0, 5))
                typevisitline.update()
            })
        }

        function init() {
            console.log('init');

            let hourtypevisit = {
                // labels: res.data.map(data => data.hous.substr(0, 5)),
                datasets: [
                    {
                        label: 'appointment',
                        backgroundColor: "#FFB6C1",
                        borderColor: "#FFB6C1",
                        // data: res.data.map(data => data.oapp),
                        fill: false
                    },
                    {
                        label: 'walkin',
                        backgroundColor: "#ADFF2F",
                        borderColor: "#ADFF2F",
                        // data: res.data.map(data => parseInt(data.vn) - parseInt(data.oapp)),
                        fill: false
                    },
                ]
            };

            let ctx = document.getElementById('typevisitline').getContext("2d");
            typevisitline = new Chart(ctx, {
                type: 'line',
                data: hourtypevisit,
                tooltips: false,
                options: {
                    plugins: {
                        datalabels: {
                            color: 'black',
                            display: function (context) {
                                return context.dataset.data[context.dataIndex] > 0;
                            },
                            font: {
                                weight: 'bold'
                            },

                            title: false,
                            align: function (context) {
                                console.log('context', context);
                                return 'top'
                            },
                            anchor: 'end',
                            display: true,
                        }
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 10
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 20
                            }
                        }]
                    },
                    legend: {
                        display: 'auto',
                        labels: {
                            fontSize: 16
                        },
                        align: 'start'
                    }
                }
            });
        }

    });
}
