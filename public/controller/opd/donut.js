export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/opd/donut", { templateUrl: "page/opd/donut.html", controller: "donut", title: "donut" })
    });
    app.controller('donut', function ($scope, $http, $resource, $q) {
        let typevisit = $http.get('/opd/typevisit')
        $q.all([typevisit]).then(result => {
            console.log(result[0].data[0]);


            let walkin = result[0].data[0].walkin
            let appointment = result[0].data[0].appointment
            let allward = {
                datasets: [{
                    fill: true,
                    data: [walkin - appointment, appointment],
                    backgroundColor: [
                        '#0008FF',
                        '#8BFF00'
                    ]
                }],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'walkin',
                    'appointment'
                ]
            };

            let ctx3 = document.getElementById("donut").getContext("2d");
            new Chart(ctx3, {
                type: 'doughnut',
                data: allward,
                tooltips: false,
                options: {
                    plugins: {
                        datalabels: {
                            label: 'Active bed',
                            color: '#fff',
                            display: function (context) {
                                return context.dataset.data[context.dataIndex] > 0;
                            },
                            font: {
                                weight: 'bold'
                            },
                            // formatter: Math.round,
                            FontSize: '20',
                            title: false
                        }
                    },
                    legend: {
                        display: true,
                        labels: {
                            fontSize: 16
                        },
                        align: 'center'
                    }
                }
            });

        })
    });
}