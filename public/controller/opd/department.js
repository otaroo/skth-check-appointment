export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/opd/department", {
      templateUrl: "page/opd/department.html",
      controller: "departmentvisit",
      title: "departmentvisit",
    });
  });

  app.controller("departmentvisit", function (
    $scope,
    $http,
    $resource,
    $q,
    $interval
  ) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    let departmentvisit;
    $scope.itemSelect = moment().format("YYYY-MM-DD");
    init();

    getData($scope.itemSelect);
    $scope.timeNow = moment().format("HH:mm");

    $interval(function () {
      getData($scope.itemSelect);
      $scope.timeNow = moment().format("HH:mm");
    }, 5000);

    function getData(date) {
      let sendData = {
        date: date,
      };
      let typevisit = $http.get("/opd/departmentvisit", sendData);

      $q.all([typevisit]).then((result) => {
        departmentvisit.data.datasets[0].data = result[0].data.map(
          (data) => data.sign
        );
        departmentvisit.data.datasets[1].data = result[0].data.map(
          (data) => data.wait
        );
        departmentvisit.data.labels = result[0].data.map(
          (data) => data.department
        );
        departmentvisit.update();
      });
    }

    function init() {
      let hourtypevisit = {
        // labels: res.data.map(data => data.hous.substr(0, 5)),
        datasets: [
          {
            type: "line",
            label: "ตรวจแล้ว",
            backgroundColor: "#ADFF2F",
            borderColor: "#ADFF2F",
            // data: res.data.map(data => parseInt(data.vn) - parseInt(data.oapp)),
            fill: false,
            datalabels: {
              display: "auto",
            },
          },
          {
            label: "รอตรวจ",
            backgroundColor: "#FFB6C1",
            borderColor: "#FFB6C1",
            // data: res.data.map(data => data.oapp),
            fill: false,
            datalabels: {
              display: "auto",
            },
          },
        ],
      };

      let ctx = document.getElementById("department").getContext("2d");
      departmentvisit = new Chart(ctx, {
        type: "bar",
        data: hourtypevisit,
        tooltips: false,
        options: {
          plugins: {
            datalabels: {
              color: "black",
              display: "auto",
              font: {
                weight: "bold",
              },

              title: false,
              align: "top",
              anchor: "end",
              display: true,
            },
          },
          scales: {
            xAxes: [
              {
                ticks: {
                  fontSize: 16,
                  padding: 10,
                },
              },
            ],
            yAxes: [
              {
                ticks: {
                  fontSize: 16,
                  padding: 20,
                },
              },
            ],
          },
          legend: {
            display: "auto",
            labels: {
              fontSize: 16,
            },
            align: "start",
          },
        },
      });
    }
  });
};
