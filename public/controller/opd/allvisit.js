export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/opd/allvisit", {
      templateUrl: "page/opd/allvisit.html",
      controller: "allvisit",
      title: "allvisit",
    });
  });
  app.controller("allvisit", function ($scope, $http, $resource) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    $scope.timeToday = moment().format("HH:mm");

    Swal.fire({
      title: "กำลังทำงาน",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      },
    });

    $http
      .get("/opd/allvisit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });

        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "Visit",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };

        lineChart("opdvisitall", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/opd/skthvisit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });

        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "Visit (ในโรงพยาบาล)",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("opdvisitskth", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/opd/puivisit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });

        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "คลินิกโรคหวัด Visit",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("puivisit", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/opd/puiroomvisit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });

        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "คนไข้ห้อง PUI",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };

        lineChart("puiroomvisit", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/ipt/puiwardvisit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });

        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "รพ.สุโขทัย",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.skt),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
            {
              label: "รพ.สนาม",
              backgroundColor: "#FF0046",
              borderColor: "#FF0046",
              fill: false,
              data: res.data.map((data) => data.temp),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("puiwardvisit", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/opd/screen226visit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });
        console.log(res.data);
        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "คนไข้ ห้องตรวจ screening",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("screenvisit", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/opd/screen248visit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });
        console.log(res.data);
        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "คนไข้ ห้องตรวจ คัดกรองโควิด-19",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("screen248visit", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));

    $http
      .get("/opd/vaccinevisit")
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });
        console.log(res.data);
        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "คนไข้ ห้องฉีดวัคซีน COVID-19",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.count),
              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("vaccinevisit", allward);
        $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));
  });
};
