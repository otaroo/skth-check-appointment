export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/opddashbord", {
      templateUrl: "page/opd/dashbord.html",
      controller: "opddashbord",
      title: "opddashbord",
    });
  });

  app.controller("opddashbord", function ($scope, $http, $resource, $q) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    $scope.timeToday = moment().format("HH:mm");

    let typevisitline;
    let typevisitChart;
    let refervisitChart;
    let referinvisitChart;
    let referoutvisitChart;
    initChart();

    let sendData = {
      date:  moment().format("YYYY-MM-DD"),
    };
    let hourvisit = $http.post("/opd/hourvisit", sendData);
    let typevisit = $http.post("/opd/typevisit", sendData);
    let hourtypevisit = $http.post("/opd/hourtypevisit", sendData);
    let refervisit = $http.post("/opd/getReferVisit", sendData);
    let referinvisit = $http.post("/opd/getReferInVisit", sendData);
    let referoutvisit = $http.post("/opd/getReferOutVisit", sendData);
    $http.post("/opd/getTop5Diag", sendData).then( (res) =>{
      $scope.opddiag = res.data;
    });

    $q.all([hourvisit, typevisit, hourtypevisit,refervisit,referinvisit,referoutvisit]).then((result) => {
      let walkin = result[1].data[0].walkin;
      let appointment = result[1].data[0].appointment;
      typevisitChart.data.datasets[0].data[0] = walkin - appointment;
      typevisitChart.data.datasets[0].data[1] = appointment;
      typevisitChart.data.datasets[0].datalabels = {
        formatter: function (value) {
          let per = (value / walkin) * 100;
          return value + ` (${per.toFixed(2)}%)`;
        },
      };
      typevisitChart.update();

      let referin = result[3].data[0].count;
      let referout = result[3].data[1].count;
      refervisitChart.data.datasets[0].data[0] = referin;
      refervisitChart.data.datasets[0].data[1] = referout;
      let referall =parseInt(referin) + parseInt(referout)
      refervisitChart.data.datasets[0].datalabels = {
        formatter: function (value) {
          let per = (value / referall) * 100;
          return value + ` (${per.toFixed(2)}%)`;
        },
      };
      refervisitChart.update();

      let referinipt = result[4].data[0].ipt;
      let referinopd = result[4].data[0].ovst;
      referinvisitChart.data.datasets[0].data[0] = referinipt;
      referinvisitChart.data.datasets[0].data[1] = referinopd;
      let referinall = parseInt(referinipt) + parseInt(referinopd)
      $scope.referin = referinall;
      referinvisitChart.data.datasets[0].datalabels = {
        formatter: function (value) {
          let per = (value / referinall) * 100;
          return value + ` (${per.toFixed(2)}%)`;
        },
      };
      referinvisitChart.update();

      let referoutipt = result[5].data[0].ipt;
      let referoutopd = result[5].data[0].ovst;
      referoutvisitChart.data.datasets[0].data[0] = referoutipt;
      referoutvisitChart.data.datasets[0].data[1] = referoutopd;
      let referoutall = parseInt(referoutipt) + parseInt(referoutopd)
      $scope.referout = referoutall;
      referoutvisitChart.data.datasets[0].datalabels = {
        formatter: function (value) {
          let per = (value / referoutall) * 100;
          return value + ` (${per.toFixed(2)}%)`;
        },
      };
      referoutvisitChart.update();


      typevisitline.data.datasets[0].data = result[2].data.map(
        (data) => data.oapp
      );
      typevisitline.data.datasets[1].data = result[2].data.map(
        (data) => parseInt(data.vn) - parseInt(data.oapp)
      );
      typevisitline.data.labels = result[2].data.map((data) =>
        data.hous.substring(0, 5)
      );
      typevisitline.update();
    });

    function initChart() {
      let typevisit = {
        datasets: [
          {
            fill: true,
            backgroundColor: ["#ADFF2F", "#FFB6C1"],
          },
        ],
        labels: ["walkin", "appointment"],
      };
      typevisitChart = pieChart("typevisit", typevisit);

      let refervisit = {
        datasets: [
          {
            fill: true,
            backgroundColor: ["#76448A", "#AF601A"],
          },
        ],
        labels: ["รับ Refer", "ส่ง Refer"],
      };
      refervisitChart = pieChart("refervisit", refervisit);

      let referinvisit = {
        datasets: [
          {
            fill: true,
            backgroundColor: ["#76448A", "#AF601A"],
          },
        ],
        labels: ["IPD", "OPD"],
      };
      referinvisitChart = pieChart("referinvisit", referinvisit);

      let referoutvisit = {
        datasets: [
          {
            fill: true,
            backgroundColor: ["#76448A", "#AF601A"],
          },
        ],
        labels: ["IPD", "OPD"],
      };
      referoutvisitChart = pieChart("referoutvisit", referoutvisit);



      let hourtypevisit = {
        datasets: [
          {
            label: "appointment",
            backgroundColor: "#FFB6C1",
            borderColor: "#FFB6C1",
            datalabels: {
              display: "auto",
            },
          },
          {
            label: "walkin",
            backgroundColor: "#ADFF2F",
            borderColor: "#ADFF2F",
            datalabels: {
              display: true,
            },
          },
        ],
      };
      typevisitline = lineChart("typevisitline", hourtypevisit);
    }
  });
};
