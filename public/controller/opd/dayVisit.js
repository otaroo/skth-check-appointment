export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/opd/dayvisit", { templateUrl: "page/opd/dayvisit.html", controller: "dayvisit", title: "dayvisit" })
    });
    app.controller('dayvisit', function ($scope, $http, $resource, $q, $timeout) {
        let dayvisitChart
        let dayvisit = $http.post('/opd/getOpdVisitByDep')
        $q.all([dayvisit]).then(result => {
            Swal.close()
            Swal.fire({
                title: 'สำเร็จ',
                type: 'success',
                timer: 2000
            })
            $scope.item = result[0].data;

            dayvisitChart.data.datasets[0].data = result[0].data.map(data => data.t_shop)            
            dayvisitChart.data.datasets[1].data = result[0].data.map(data => data.t079)            
            dayvisitChart.data.datasets[2].data = result[0].data.map(data => data.t150)            
            dayvisitChart.data.datasets[3].data = result[0].data.map(data => data.t_pcu)            
            dayvisitChart.data.datasets[4].data = result[0].data.map(data => data.t_all)            
            dayvisitChart.data.labels = result[0].data.map(data => moment(data.gdate).format("DD/MM/YY") )
            dayvisitChart.update();
           
        });
        initChart();
        function initChart() {
            let dayvisit = {
                datasets: [
                    {
                        type: 'line',
                        label: 'รับยาร้านยา',
                        borderColor: "rgba(65,105,225,0.7)",
                        backgroundColor: "rgba(65,105,225,0.7)",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        },

                    },
                    {
                        type: 'line',
                        label: 'Refill ประกันสังคม',
                        borderColor: "#FF191F",
                        backgroundColor: "#FF191F",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        },

                    },
                    {
                        type: 'line',
                        label: 'เวชกรรมสังคม',
                        borderColor: "rgba(51,51,1,0.7)",
                        backgroundColor: "rgba(51,51,1,0.7)",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        },

                    },
                    {
                        type: 'line',
                        label: 'แผนไทยริมน้ำ',
                        borderColor: "#839192",
                        backgroundColor: "#839192",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        },

                    },
                    {
                        type: 'bar',
                        label: 'ทั้งหมด',
                        borderColor: "rgba(255,200,2,0.7)",
                        backgroundColor: "rgba(255,200,2,0.7)",
                        fill: false,
                        datalabels: {
                            display: true,
                            align: 'end',
                            anchor: 'end'
                        },

                    }
                ]
            };
            dayvisitChart = barChart('dayvisit', dayvisit);
        }
    });
}