export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/opd/opdmed", {
      templateUrl: "page/opd/opdmed.html",
      controller: "opdmed",
      title: "opdmed",
    });
  });
  app.controller("opdmed", function ($scope, $http, $resource, $q, $timeout) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    // $scope.timeNow = moment().format("HH:mm");

    let hourmedvisitChart;
    Swal.fire({
      title: "กำลังทำงาน",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      },
    });
    let opdmed = $http.get("/opd/hourmedvisit");
    $q.all([opdmed]).then((result) => {
      console.log(result);
      Swal.close();
      Swal.fire({
        title: "สำเร็จ",
        // text: '',
        type: "success",
        timer: 2000,
      });

      $scope.visit = result[0].data.reduce((sum, number) => {
        return sum + parseInt(number.vn);
      }, 0);

      hourmedvisitChart.data.datasets[0].data = result[0].data.map(
        (data) => data.doctor
      );
      hourmedvisitChart.data.datasets[1].data = result[0].data.map(
        (data) => data.vn
      );
      hourmedvisitChart.data.labels = result[0].data.map((data) =>
        data.hous.substring(0, 5)
      );
      hourmedvisitChart.update();
    });

    let hourmedvisit = {
      datasets: [
        {
          type: "line",
          label: "Doctor",
          borderColor: "rgba(25,48,255,0.7)",
          backgroundColor: "rgba(25,48,255,0.7)",
          fill: false,
          datalabels: {
            display: function (context) {
              var index = context.dataIndex;
              if (context.dataset.data[index] <= 0) {
                return false;
              } else if (
                context.dataset.data[index] > 0 &&
                context.dataset.data[index] < 30
              ) {
                return "auto";
              }
              if (context.dataset.data[index] >= 30) {
                return true;
              }
            },

            align: "top",
            anchor: "start",
          },
        },
        // {
        //     type: 'line',
        //     label: 'Admit',
        //     borderColor: "#FF191F",
        //     backgroundColor: "#FF191F",
        //     fill: false,
        //     datalabels: {
        //         display: false,
        //         // display: function (context) {
        //         //     var index = context.dataIndex;
        //         //     return context.dataset.data[index] > 0 ? 'auto'  : false;
        //         // },
        //         align: 'top',
        //         anchor: 'end'
        //     }
        // },
        // {
        //     type: 'line',
        //     label: 'Dispense',
        //     borderColor: "rgba(51,51,1,0.7)",
        //     backgroundColor: "rgba(51,51,1,0.7)",
        //     fill: false,
        //     datalabels: {
        //         display: function (context) {
        //             var index = context.dataIndex;
        //             if (context.dataset.data[index] <= 0) {
        //                 return false
        //             } else if (context.dataset.data[index] > 0 && context.dataset.data[index] < 30) {
        //                 return 'auto'
        //             }
        //             if (context.dataset.data[index] >= 30) {
        //                 return true
        //             }
        //         },
        //         align: 'top',
        //         anchor: 'end'
        //     }
        // },
        // {
        //     type: 'line',
        //     label: 'Finance',
        //     borderColor: "#839192",
        //     backgroundColor: "#839192",
        //     fill: false,
        //     datalabels: {
        //         display: 'auto',
        //         // display: function (context) {
        //         //     var index = context.dataIndex;
        //         //     return context.dataset.data[index] > 0 ? 'auto'  : false;
        //         // },
        //         align: 'top',
        //         anchor: 'end'
        //     }
        // },
        {
          type: "bar",
          label: "ซักประวัติอายุรกรรม",
          borderColor: "rgba(255,200,2,0.7)",
          backgroundColor: "rgba(255,200,2,0.7)",
          fill: false,
          datalabels: {
            display: "auto",
            align: "end",
            anchor: "end",
          },
        },
      ],
    };
    hourmedvisitChart = barChart("hourmedvisit", hourmedvisit);
  });
};
