app.config(function ($routeProvider) {
  $routeProvider
    .when("/sks/upload", { templateUrl: "page/sks/upload.html", controller: 'upload', title: 'upload' })
    .when("/sks/debt", { templateUrl: "page/sks/debt.html", controller: 'sksdebt', title: 'sksdebt' })
    .when("/sks/summary", { templateUrl: "page/sks/summary.html", controller: 'summary', title: 'summary' })
    .when("/sks/compare", { templateUrl: "page/sks/compare.html", controller: 'compare', title: 'compare' })
    .when("/sks/sks", { templateUrl: "page/sks/sks.html", controller: 'sks', title: 'sks' })
});


app.controller('sks', function ($scope, $http, $resource, $timeout) {
  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
  $scope.series = ['Series A', 'Series B'];
  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };

  $http.get('/debtor/getChart').then((res) => {

    $scope.vstdate = res.data.vstdate
    $scope.count = res.data.count
    $timeout(function () {
      $scope.labels = res.data.vstdate
      $scope.data = [
        res.data.count
      ];
    }, 3000);
  });
});

app.controller('upload', function ($scope, $http, $resource, ) {

  $http.get('/debtor/listStatement').then((ress) => {
    $scope.listStatement = ress.data.statement
    $resource($scope.listStatement).$promise;
  });
  $scope.dtOptions = {
    paginationType: 'full_numbers',
    displayLength: 10,
    lengthChange: false,
    language: {
      info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
      paginate: {
        previous: "ก่อนหน้า",
        next: "ถัดไป"
      },
      search: 'ค้นหา'
    },
  }
  $scope.beforeUpload = function () {

    document.querySelector('#Upload').reset();
    $scope.upload_message = '';
    $scope.upload_show = false;
  }
  $scope.upload = function (file) {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    
    var file = $scope.statement;
    var fd = new FormData();
    fd.append('file', file);

    $http.post('/debtor/upload', fd, { headers: { 'Content-Type': undefined } }).then((res) => {

      document.querySelector('#Upload').reset();
      Swal.close()
      Swal.fire({
        title: res.data.isError,
        text: res.data.message,
        type: res.data.isError,
        timer: 2000
      })
      setTimeout(function () {
        $scope.$apply(function () {
          $http.get('/debtor/listStatement').then((ress) => {
            $scope.listStatement = ress.data.statement
            $resource($scope.listStatement).$promise;
          });
        });
      }, 2000);
    })
    $scope.statement = '';
  }

  $scope.editbill = function (item) {
    $scope.show = false;
    $scope.edit_message = '';
    $scope.edit = {}
    $scope.edit.statement = item.statement_no || ''
    $scope.edit.bill_vol = item.bill_vol || ''
    $scope.edit.bill_no = item.bill_no || ''
    if (item.bill_date) {
      let d = new Date(item.bill_date);
      var dd = d.getDate();
      var mm = d.getMonth() + 1; //January is 0!
      var yyyy = d.getFullYear();
      $scope.edit.bill_date = dd + '/' + mm + '/' + yyyy;
      $(".datepicker").datepicker("update", $scope.edit.bill_date);
    } else {
      $(".datepicker").datepicker("update", '');
    }
    $('#editModal').modal('show');
  }

  $scope.bill = function () {

    let data = {
      statement: $scope.edit.statement,
      vol: $scope.bill_vol,
      no: $scope.bill_no,
      date: $scope.bill_date,
    }
    $http.post('/debtor/bill', data).then((res) => {

      Swal.fire({
        title: 'สำเร็จ',
        text: 'บันทึกสำเร็จ',
        type: 'success',
        timer: 2000
      })
    })
    setTimeout(function () {
      $('#editModal').modal('hide');

      $scope.$apply(function () {
        $http.get('/debtor/listStatement').then((ress) => {
          $scope.listStatement = ress.data.statement
          $resource($scope.listStatement).$promise;
        });
      });
    }, 2000);

  }
})

app.directive('fileModel', function ($parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;
      element.bind('change', function () {
        scope.$apply(function () {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
});

app.controller('sksdebt', function ($scope, $http, $resource) {
  $scope.noinvno = "true";
  $scope.dtOptions = {
    paginationType: 'full_numbers',
    displayLength: 10,
    lengthChange: false,
    language: {
      info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
      paginate: {
        previous: "ก่อนหน้า",
        next: "ถัดไป"
      },
      search: 'ค้นหา'
    },
    dom: 'Bfrtip',
    buttons: [{
      extend: 'excel',
      text: 'excel',
      title: '',
      filename: 'รายการลูกหนี้',
      className: 'btn-primary'
    }]
  }

  $scope.searchDebt = function () {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let data = {
      "hn": $scope.hn,
      "datestart": $scope.datestart,
      "dateend": $scope.dateend,
      "noinvno": $scope.noinvno,
      "nostatement": $scope.nostatement,
      "pttype": $scope.pttype,
    };

    $http.post('/debtor/listDebt', data).then((res) => {
      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.mseeage,
        type: 'success',
        timer: 2000
      })
      $scope.debt = res.data.debt
      $resource($scope.debt).$promise;
    })
  }
})

app.controller('summary', function ($scope, $http, $resource, ) {
  $scope.search = function () {
     Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let data = {
      "datestart": $scope.datestart,
      "dateend": $scope.dateend,
      "pttype": $scope.pttype
    };
    $http.post('/debtor/listDebtCount', data).then((res) => {
      $scope.row = res.data.row

      $resource($scope.row).$promise;
      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.mseeage,
        type: 'success',
        timer: 2000
      })
    })
  }
  $scope.excel = function () {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let data = {
      "datestart": $scope.datestart,
      "dateend": $scope.dateend,
      "pttype": $scope.pttype
    };
    $http.post('/debtor/exportDebtExcel', data,{
      responseType: 'arraybuffer'
    }).then((res) => {
      var blob = new Blob([res.data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      saveAs(blob, "Excel.xlsx")
      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.mseeage,
        type: 'success',
        timer: 2000
      })
    })
  }

});


app.controller('compare', function ($scope, $http, $resource, ) {
  $scope.dtOptions = {
    paginationType: 'full_numbers',
    displayLength: 10,
    lengthChange: false,
    language: {
      info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
      paginate: {
        previous: "ก่อนหน้า",
        next: "ถัดไป"
      },
      search: 'ค้นหา'
    }
  }
  $http.get('/debtor/listStatement').then((ress) => {
    $scope.listStatement = ress.data.statement
    $scope.listStatement.forEach(element => {
      if ((parseInt(element.statement_count) - parseInt(element.debt)) == 0) {
        element.disabled = true
      } else {
        element.disabled = false
      }

    });
    $resource($scope.listStatement).$promise;
  });

  $scope.debt = function (item) {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })

    $scope.statement_id = item.statement_id || ''
    let data = {
      statement_id: $scope.statement_id
    }
    $http.post('/debtor/debt', data).then((res) => {
      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.mseeage,
        type: 'success',
        timer: 2000
      })
      setTimeout(function () {
        $scope.$apply(function () {
          $http.get('/debtor/listStatement').then((ress) => {
            $scope.listStatement = ress.data.statement
            $scope.listStatement.forEach(element => {
              if ((parseInt(element.statement_count) - parseInt(element.debt)) == 0) {
                element.disabled = true
              } else {
                element.disabled = false
              }
        
            });
            $resource($scope.listStatement).$promise;
           
          });
        });
      }, 2000);
    })
  }


});

