export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/approval", {
      templateUrl: "page/register/approval.html",
      controller: "approval",
      title: "approval",
    });
  });

  app.controller(
    "approval",
    function ($scope, $http, $localStorage, $location) {
      console.log($localStorage.cid);
      if (!$localStorage.cid) {
        $location.path("/");
      }
      $scope.accept = function () {
        console.log($scope.agree);
        if ($scope.agree == 1) {
          $location.path("/register");
        } else if ($scope.agree == 2) {
          $location.path("/");
        }
      };
    }
  );
};
