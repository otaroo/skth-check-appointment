export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/register", {
      templateUrl: "page/register/register.html",
      controller: "register",
      title: "register",
    });
  });

  app.controller(
    "register",
    function ($scope, $http, $q, $localStorage, $location) {
      if (!$localStorage.cid) {
        $location.path("/");
      }
      $scope.cid = $localStorage.cid;
      $scope.token_reCaptcha;
      $scope.user = {
        cid: $scope.cid,
        religion: "",
        chwpart: "64",
        amppart: "01",
        // birthday: moment().subtract(20, "year"),
      };
      
      
      $scope.getLabel = (name) =>{
        let title = document.getElementsByName(name.$name);
        let parent =  $(title).parent()
        return $(parent).prev().text()
      }

      let occupation = $http.get("/register/occupation");
      let religion = $http.get("/register/religion");
      let chwpart = $http.get("/register/chwpart");
      let amppart = $http.get("/register/amppart");
      let tmppart = $http.get("/register/tmppart");
      let marrystatus = $http.get("/register/marrystatus");

      $q.all([
        occupation,
        religion,
        chwpart,
        amppart,
        tmppart,
        marrystatus,
      ]).then((result) => {
        Swal.close();
        $scope.occupationList = result[0].data;
        $scope.religionList = result[1].data;
        $scope.chwpartList = result[2].data;
        $scope.amppartList = result[3].data;
        $scope.tmppartList = result[4].data;
        $scope.statusList = result[5].data;

        grecaptcha.ready(function () {
          console.log("grecaptcha");
        });
      });

      $scope.save = async () => {
        $scope.token_reCaptcha = await grecaptcha.execute(
          "6LeaIpkbAAAAAG6hlvSZGlkpjJ_sXg4ETkX8eS8Q",
          {
            action: "submit",
          }
        );
        if (!$scope.token_reCaptcha) {
          Swal.fire({
            title: "กรุณาลองใหม่",
            type: "info",
            timer: 3000,
          });
          return;
        }

        Swal.fire({
          title: "กำลังตรวจสอบ",
          allowEscapeKey: false,
          allowOutsideClick: false,
          onOpen: () => {
            Swal.showLoading();
          },
        });

        if (myForm.$invalid) {
          console.log("invalid");
          return;
        }

        let send = { token: $scope.token_reCaptcha, ...$scope.user };

        $http
          .post("/register/", send)
          .then((result) => {
            if (result.data.status) {
              Swal.fire({
                title: result.data.message,
                title: "ลงทะเบียนสำเร็จ",
                type: result.data.status ? "success" : "error",
                html:
                  "โรงพยาบาลสุโขทัยได้รับข้อมูลการลงทะเบียนของท่านเรียบร้อยแล้ว " +
                  "เมื่อถึงกำหนดวันนัดฉีดวัคซีน โรงพยาบาลสุโขทัยจะส่งวันนัดหมายฉีดวัคซีนให้ท่านผ่านระบบ SMS ตาม<b>เบอร์โทรศัพท์</b>ที่ท่านได้แจ้งไว้ในขั้นตอนการลงทะเบียน",
                timer: 30000,
                showCloseButton: true,
                onClose: () => {
                  console.log("onClose");
                  $location.path("/");
                  window.location.href = "/";
                },
              }).then(() => {
                $location.path("/");
                window.location.href = "/";
              });
            } else {
              Swal.fire({
                title: result.data.message,
                type: result.data.status ? "success" : "error",
                timer: 10000,
                showCloseButton: true,
              });
            }
          })
          .catch((error) => {
            console.log(error);
          });
      };
    }
  );
};
