app.config(function($routeProvider) {
  $routeProvider
    .when("/uc/upload", { templateUrl: "page/uc/upload.html", controller: "ucupload", title: "ucupload"})
    .when("/uc/stupload", { templateUrl: "page/uc/upload_statement.html", controller: "ucstupload", title: "ucstupload"});
});

app.directive('fileModel', function ($parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      console.log('fileModel');      
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;
      element.bind('change', function () {
        scope.$apply(function () {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
});

app.controller("ucupload", function($scope, $http, $resource) {
  $scope.beforeUpload = function() {
    document.querySelector("#ucUpload").reset();
    $scope.upload_message = "";
    $scope.upload_show = false;
  };
  $scope.upload = function(file) {
    Swal.fire({
      title: "กำลังทำงาน",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    var file = $scope.ucdebt;
    var fd = new FormData();
    fd.append("file", file);

    $http
      .post("/uc/upload", fd, { headers: { "Content-Type": undefined } })
      .then(res => {
        document.querySelector("#ucUpload").reset();
        Swal.close();
        Swal.fire({
          title: res.data.title,
          text: res.data.message,
          type: res.data.type,
          timer: 3000
        });
      });
    $scope.ucUpload = "";
  };
});

app.controller("ucstupload", function($scope, $http, $resource) {
  $scope.beforeUpload = function() {
    document.querySelector("#ucSTUpload").reset();
  };
  $scope.upload = function(file) {
    Swal.fire({
      title: "กำลังทำงาน",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    });
    var file = $scope.ucstdebt;
    var fd = new FormData();
    fd.append("file", file);

    $http
      .post("/uc/stupload", fd, { headers: { "Content-Type": undefined } })
      .then(res => {
        document.querySelector("#ucSTUpload").reset();
        Swal.close();
        Swal.fire({
          title: res.data.title,
          text: res.data.message,
          type: res.data.type,
          timer: 3000
        });
      });
    $scope.ucUpload = "";
  };
});