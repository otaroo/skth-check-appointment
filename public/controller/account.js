app.config(function ($routeProvider) {
  $routeProvider
    .when("/account/search", { templateUrl: "page/account/search.html", controller: 'search', title: 'search' })
    .when("/account/compare", { templateUrl: "page/account/compare.html", controller: 'acccompare', title: 'acccompare' })
    .when("/account/debt", { templateUrl: "page/account/debt.html", controller: 'accdebt', title: 'accdebt' })
});

app.controller('search', function ($scope, $http, $resource, $timeout) {
  $scope.dtOptions = {
    paginationType: 'full_numbers',
    displayLength: 10,
    lengthChange: false,
    language: {
      info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
      paginate: {
        previous: "ก่อนหน้า",
        next: "ถัดไป"
      },
      search: 'ค้นหา'
    },
    dom: 'Bfrtip',
    buttons: [{
      extend: 'excel',
      text: 'excel',
      title: '',
      filename: 'รายการลูกหนี้ส่งบัญชี',
      className: 'btn-primary'
    }]
  }

  $scope.search = function () {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let dataSearch = {
      "datestart": $scope.datestart,
      "pttype": $scope.pttype
    };

    $http.post('/account/getDebtPerDay', dataSearch).then((res) => {
      $scope.listDebt = res.data.row
      $resource($scope.listDebt).$promise;

      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.mseeage,
        type: 'success',
        timer: 2000
      })
    })
  };

  $scope.saveDebt = function () {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    $http.post('/account/saveDebtPerDay', $scope.listDebt).then((res) => {
      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.mseeage,
        type: 'success',
        timer: 2000
      })
    })

  }
})


app.controller('acccompare', function ($scope, $http, $resource, $timeout) {
  $scope.dtOptions = {
    paginationType: 'full_numbers',
    displayLength: 10,
    lengthChange: false,
    language: {
      info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
      paginate: {
        previous: "ก่อนหน้า",
        next: "ถัดไป"
      },
      search: 'ค้นหา'
    }
  }
  $http.get('/account/getStatement').then((res) => {
    $scope.listStatement = res.data.row
    $resource($scope.listStatement).$promise;
  })

  $scope.compareDebt = function (statement_id) {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let data = {
      statement_id: statement_id
    }
    $http.post('/account/compare', data).then((res) => {
      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.message,
        type: 'success',
        timer: 2000
      })
    })
  }
  $scope.excelStatement = function (statement_id, statement, type) {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let data = {
      statement_id: statement_id
    }
    $http.post('/account/statementNotComplete', data, {
      responseType: 'arraybuffer'
    }).then((res) => {
      var blob = new Blob([res.data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      saveAs(blob, statement + "_" + type + ".xlsx")

    })
  }

})

app.controller('accdebt', function ($scope, $http, $resource, $timeout) {
  $scope.dtOptions = {
    paginationType: 'full_numbers',
    displayLength: 10,
    lengthChange: false,
    language: {
      info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
      paginate: {
        previous: "ก่อนหน้า",
        next: "ถัดไป"
      },
      search: 'ค้นหา'
    }
  }
  $scope.search = function () {
    Swal.fire({
      title: 'กำลังทำงาน',
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      }
    })
    let data = {
      "datestart": $scope.startdate,
      "dateend": $scope.enddate,

    }
    $http.post('/account/accDebt', data).then((res) => {
      $scope.count = res.data
      $resource($scope.count).$promise;

      Swal.close()
      Swal.fire({
        title: 'สำเร็จ',
        text: res.data.message,
        type: 'success',
        timer: 2000
      })
    })
  }
  $scope.export = function (type) {
    let data = {
      "datestart": $scope.startdate,
      "dateend": $scope.enddate,
    }
    let urlType = ''; 
    let nameType = ''; 
    if (type == 1) {
      urlType = 'accDebtComplete'; 
      nameType = 'ตัดแล้ว';
    } else if (type == 2) {
      urlType = 'accDebtNotComplete'; 
      nameType = 'ยังไม่ตัด';
    }
    $http.post('/account/'+urlType, data, {
      responseType: 'arraybuffer'
    }).then((res) => {
      let blob = new Blob([res.data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      saveAs(blob, "ลูกหนี้"+nameType+".xlsx")
    })
  }

});