app.config(function ($routeProvider) {
    $routeProvider.when("/", { templateUrl: "page/ipt/dashbord.html", controller: "dashbord", title: "dashbord" })
});

Chart.plugins.register({
    id: 'paddingBelowLegends',
    beforeInit: function (chart, options) {
        chart.legend.afterFit = function () {
            this.height = this.height + 30;
        };
    }
});

app.controller('dashbord', function ($scope, $http, $resource) {
    moment.locale('th');
    let year = parseInt(moment().format('YYYY')) + 543;
    $scope.dateToday = moment().format('Do MMMM') + ' ' + year;
    $scope.timeToday = moment().format('HH:mm');
    Swal.fire({
        title: 'กำลังทำงาน',
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
            Swal.showLoading();
        }
    })

    $http.get('/ipt/alltype').then((res) => {
        Swal.close()
        Swal.fire({
            title: 'สำเร็จ',
            text: res.data.mseeage,
            type: 'success',
            timer: 2000
        })
        $scope.ipttypeTb = res.data

       

        let wardType = {
            labels: res.data.map(data => data.type_name),
            datasets: [{
                label: 'จำนวนผู้ป่วย',
                backgroundColor: "#4E73DF",

                data: res.data.map(data => data.an),
                datalabels: {
                    align: 'top',
                    anchor: 'end',
                    formatter: function (value) {
                        return value + ` (${res.data.filter((item) => { return item.an === value }).map(m => m.percent)}%)`;
                    }
                }
            }
            ]
        };
        barChart('wardType', wardType);

        $resource($scope.ipttypeTb).$promise;
    }).catch(err => console.log(err))




    $http.get('/ipt/bedrate').then((res) => {
        let wardRate = {
            labels: res.data.map(data => data.rate + ' %'),
            datasets: [{
                label: 'จำนวน Ward',
                backgroundColor: "#FF1493",
                data: res.data.map(data => data.wardRate),
                datalabels: {
                    align: 'top',
                    anchor: 'end'
                }
            }]
        };
        barChart('bedrate', wardRate);

    }).catch(err => console.log(err))

    $http.get('/ipt/allward').then((res) => {

        $scope.ipt = res.data
        $scope.sumAll = res.data.reduce((sum,number) => {
            return sum+ parseInt(number.an) 
          }, 0)
        res.data.sort((a, b) => (parseFloat(a.percent) < parseFloat(b.percent)) ? 1 : -1)
        let allward = {
            labels: res.data.map(data => data.ward),
            datasets: [{
                label: 'จำนวนผู้ป่วย',
                backgroundColor: "#4E73DF",
                data: res.data.map(data => `${data.percent}`),
                datalabels: {

                    align: 'top',
                    anchor: 'end',
                    textAlign: 'center',
                    formatter: function (value, context) {
                        let an = res.data[context.dataIndex].an;
                        let labels = `${an}\n (${value}%)`
                        return labels;
                    }
                }
            }]
        };
        barChart('allward', allward);
    }).catch(err => console.log(err))

    $http.get('/ipt/allActiveBed').then((res) => {


        let activebed = {
            labels: res.data.map(data => data.date),
            datasets: [{
                label: 'อัตราครองเตียง',
                backgroundColor: "#4E73DF",
                borderColor: "#4E73DF",
                fill: false,
                data: res.data.map(data => data.activeBed),

                datalabels: {
                    align: 'end',
                    anchor: 'end',
                    // offset: 8,
                    textAlign: 'center',
                    formatter: function (value, context) {
                        let an = res.data[context.dataIndex].count;
                        let labels = `${an} \n (${value}%)`
                        return labels;
                    }
                }
            }]
        };
        lineChart('activebed', activebed);
     
    }).catch(err => console.log(err))


    $http.get('/ipt/anallward').then((res) => {
        res.data.sort((a, b) => (parseFloat(a.an) < parseFloat(b.an)) ? 1 : -1)
        let allward = {
            labels: res.data.map(data => data.ward),
            datasets: [{
                label: 'จำนวนผู้ป่วย',
                backgroundColor: "#FF69B4",
                data: res.data.map(data => `${data.an}`),
                datalabels: {
                    align: 'top',
                    anchor: 'end'
                }
            }]
        };
        barChart('anallward', allward);
    }).catch(err => console.log(err))
})