import hourvisitController from "./opd/hourvisit.js";
import donutController from "./opd/donut.js";
import allvisitController from "./opd/allvisit.js";
import housController from "./opd/hous.js";
import departmentController from "./opd/department.js";
import opdmedController from "./opd/opdmed.js";
import opddashbord from "./opd/opddashbord.js";
import dayVisit from "./opd/dayVisit.js";

donutController();
hourvisitController();
allvisitController();
housController();
departmentController();
opdmedController();
opddashbord();
dayVisit();