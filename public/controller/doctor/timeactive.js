export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/doctor/timeactive", { templateUrl: "page/doctor/timeactive.html", controller: "timeactive", title: "timeactive" })
    });
    app.controller('timeactive', function ($scope, $http, $resource, $q, $timeout) {
        let startDate;
        let endDate;
        moment.locale('th')
        let year = parseInt(moment().format('YYYY')) + 543;
        // $scope.startDate = moment().format('Do MMMM') + ' ' + year;
        // $scope.endDate = moment().format('Do MMMM') + ' ' + year;
        // $scope.timeToday = moment().format('HH:mm');

        let dateOption = moment().format('Do MMMM') + ' ' + year
        let dateValue = moment().format('YYYY-MM-DD')
        $scope.dateOption = [{
            value: dateValue,
            name: dateOption,
            selected: true
        }]

        $scope.itemSelect = dateOption
        for (let index = 1; index <= 7; index++) {
            dateOption = moment().subtract(index, 'days').format('Do MMMM') + ' ' + year
            dateValue = moment().subtract(index, 'days').format('YYYY-MM-DD')
            $scope.dateOption.push({
                value: dateValue,
                name: dateOption,
                selected: false
            })
        }

        let timeactiveChart
        initChart($scope.itemSelect);

        $scope.selectStartDate = function (date) {
            $("#endDate").datepicker('update', '');
            $scope.endDate = '';
            if (!date) return;
            var months_th = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",];
            let dateArr = date.split(" ");
            let month = months_th.findIndex(f => f == dateArr[1]) + 1
            startDate = `${(parseInt(dateArr[2]) - 543)}-${month.toString().padStart(2, '0')}-${dateArr[0]}`
            $("#endDate").datepicker("setStartDate",new Date(startDate));

        }
        $scope.selectEndDate = function (date) {
            if (!date) return;
            var months_th = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",];
            let dateArr = date.split(" ");
            let month = months_th.findIndex(f => f == dateArr[1]) + 1
            endDate = `${(parseInt(dateArr[2]) - 543)}-${month.toString().padStart(2, '0')}-${dateArr[0]}`
            getChartData();
        }

        function getChartData() {
            Swal.fire({
                title: 'กำลังทำงาน',
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            })
            let sendData = {
                startDate: startDate,
                endDate : endDate
            }
            let timeactive = $http.post('/doctor/doctorActive', sendData)
            $q.all([timeactive]).then(result => {
                Swal.close()
                Swal.fire({
                    title: 'สำเร็จ',
                    type: 'success',
                    timer: 2000
                })
                let convertResult  = Object.entries(result[0].data[0]);
                timeactiveChart.data.datasets[0].data = convertResult.map(data => data[1])
                // timeactiveChart.data.labels = ['00:01 - 08:29','00:01 - 08:29','00:01 - 08:29','00:01 - 08:29','00:01 - 08:29','00:01 - 08:29']
                timeactiveChart.update();
            })
        }

        function initChart() {
            let timeactive = {
                labels: ['00:01-08:29','08:30-11:59','12:00-12:59','13:00-16:29','16:30-20:29','20:30-23:59'],
                datasets: [
                    {
                        type: 'line',
                        label: 'Doctor',
                        borderColor: "#FF191F",
                        backgroundColor: "#FF191F",
                        fill: false,
                        datalabels: {
                            display: true,
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    
                ]
            };
            let ctx = document.getElementById('timeactive').getContext('2d');
            timeactiveChart = new Chart(ctx, {
                type: 'bar',
                data: timeactive,
                options: {
                    responsive: true,
                    plugins: {
                        datalabels: {

                            color: 'black',
                            display: function (context) {
                                return context.dataset.data[context.dataIndex] > 0;
                            },
                            font: {
                                weight: 'bold'
                            },
                            FontSize: '20',
                            title: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 10
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 20
                            }
                        }]
                    },
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16
                        },
                        align: 'start'
                    }
                }
            });
        }
    });
}