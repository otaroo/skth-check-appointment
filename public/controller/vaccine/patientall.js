export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/vaccine/patientall", {
      templateUrl: "page/vaccine/patientall.html",
      controller: "patientall",
      title: "patientall",
    });
  });
};

app.controller("patientall", function ($scope, $http, $resource) {
  $scope.patientAll;

  let sendData = {
    Date: moment().format("YYYY-MM-DD"),
  };

  function patient() {
    $http.post("/vaccine/patientall", sendData).then((result) => {
      $scope.patientAll = result.data;
      console.log(result.data);
    });
  }
  patient();

  setInterval(() => {
    patient();
  }, 10000);
});
