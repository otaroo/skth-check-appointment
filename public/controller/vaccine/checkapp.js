export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/", {
      templateUrl: "page/vaccine/checkapp.html",
      controller: "checkapp",
      title: "checkapp",
    });
  });

  app.controller(
    "checkapp",
    function ($scope, $http, $localStorage, $location) {
      $localStorage.$reset();
      $scope.$storage = $localStorage;
      // $scope.$storage = $localStorage.$default({
      //   x: 42
      // });
      $scope.token_reCaptcha;
      grecaptcha.ready(function () {
        grecaptcha
          .execute("6LeaIpkbAAAAAG6hlvSZGlkpjJ_sXg4ETkX8eS8Q", {
            action: "submit",
          })
          .then(function (token) {
            // Add your logic to submit to your backend server here.
            $scope.token_reCaptcha = token;
          });
      });

      $scope.cid = "";
      $scope.appointment = [];
      moment.locale("th");
      let year = parseInt(moment().format("YYYY")) + 543;
      $scope.dateToday = moment().format("Do MMMM") + " " + year;

      $scope.change = () => {
        $scope.isInputChange = false;
      };

      $scope.canAppointment = false;

      $scope.check = function (cid) {
        $scope.canAppointment = false;
        
        grecaptcha
          .execute("6LeaIpkbAAAAAG6hlvSZGlkpjJ_sXg4ETkX8eS8Q", {
            action: "submit",
          })
          .then(function (token) {
            // Add your logic to submit to your backend server here.
            $scope.token_reCaptcha = token;
          });

        if (!$scope.token_reCaptcha) {
          console.log("กรุณาตรวจสอบ");
          Swal.fire({
            title: "กรุณาลองใหม่",
            type: "info",
            timer: 3000,
          });
          return;
        }

        Swal.fire({
          title: "กำลังตรวจสอบ",
          allowEscapeKey: false,
          allowOutsideClick: false,
          onOpen: () => {
            Swal.showLoading();
          },
        });

        let data = {
          cid: cid,
          token: $scope.token_reCaptcha,
        };

        $http.post("/moph/checkapp", data).then((result) => {
          let moph = result.data;
          $scope.appointment = moph.confirm_appointment_slot || [];
          $scope.queue = moph.queue || {};
          $scope.person = {};
          $scope.error = false;
          $scope.skt = "";
          $scope.mod11 = true;
          $scope.recap = true;
          

          if (moph.recap != undefined) {
            $scope.recap = moph.recap;
            console.log($scope.recap);
          }
          if (moph.mod11 != undefined) {
            $scope.mod11 = moph.mod11;
            console.log($scope.mod11);
          }
          if (moph.sktdate) {
            let year = parseInt(moment(moph.sktdate).format("YYYY")) + 543;
            $scope.skt = moment(moph.sktdate).format("Do MMMM ") + year;
          }

          $scope.isAppointment1 = $scope.appointment.some(function (arrVal) {
            return 1 === arrVal.dose;
          });
          $scope.isAppointment2 = $scope.appointment.some(function (arrVal) {
            return 2 === arrVal.dose;
          });

          $scope.person = {
            prefix: moph.prefix,
            first_name: moph.first_name,
            last_name: moph.last_name,
            person_type_name: moph.person_type_name,
          };

          $scope.appointment.forEach((item) => {
            let year =
              parseInt(moment(item.appointment_date).format("YYYY")) + 543;
            item.appointment_date =
              moment(item.appointment_date).format("DD MMMM") + " " + year;
            item.appointment_time = item.appointment_time.substring(0, 5);
          });
          $scope.isInputChange = true;
          Swal.close();
          Swal.fire({
            title:moph.message,
            timer: 5000,
            showCloseButton: false,
            onClose: () => {
              
              if(moph.btnapp){
                $scope.register(cid)
              }
            },
          });
        });
      };

      $scope.register = function (cid) {
        let data = {
          cid: cid,
          token: $scope.token_reCaptcha,
        };

        $http.post("/register/checkmoph", data).then((result) => {
          if (result.data.status) {
            console.log(result.data);
            $scope.canAppointment = true;
          } else {
            Swal.fire({
              title: result.data.hos_name || result.data.vaccine,
              html: result.data.message,
              type: "error",
              timer: 20000,
              showCloseButton: true,
              closeButtonAriaLabel: "ปิด",
            });
          }
        });
      };
      $scope.registerBtn = (cid) => {
        $scope.$storage.cid = cid;
        $location.path("/approval");
      }

    }
  );
};
