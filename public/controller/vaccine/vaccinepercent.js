
export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/vaccine/vaccinepercent", {
      templateUrl: "page/vaccine/vaccinepercent.html",
      controller: "vaccinepercent",
      title: "vaccinepercent",
    });
  });

  app.controller("vaccinepercent", function ($scope, $http, $resource) {
    $scope.personCount = '71644';
    let data = {
        "startDate" : "2021-04-01",
        "endDate" : moment().format("YYYY-MM-DD"),
        "personCount" :  $scope.personCount
    }
    $http
      .post("/vaccine/vaccinepercent",data)
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });
        console.log(res);
        let vaccine = {
          labels: res.data.map((data) =>  moment(data.date).format("DD/MM")),
          datasets: [
            {
              label: "% ความครอบคลุมการฉีดวัคซีน ของประชากร อ.เมือง จ.สุโขทัย คิดจากประชากรที่ฉีด Sinovac ครบ 2เข็ม + Astra เข็มที่ 1",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) => data.percent),
              datalabels: {
                align: "top",
                anchor: "end",
                formatter: function (value) {
                    return value + ` (%)`;
                }
              },
            },
          ],
        };

        lineChart("vaccinepercent", vaccine);
        // $scope.$apply();
        // $resource($scope.ipt).$promise;
      })
      .catch((err) => console.log(err));
  })
};


