export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/vaccine/patientfail", {
      templateUrl: "page/vaccine/patientfail.html",
      controller: "patientfail",
      title: "patientfail",
    });
  });
  app.controller("patientfail", function ($scope, $http, $resource) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    $scope.patient = [];
    $scope.wrongdep = [];
    $scope.visitfail = [];

    let sendData = {
      // Date: moment().format("2021-05-21"), // ทดสอบ
      Date: moment().format("YYYY-MM-DD"),
    };

    function patient() {
      $http.post("/vaccine/patientfail", sendData).then((result) => {
        $scope.patient = result.data;
        console.log(result.data);
      });
    }
    function getwrongdep() {
      $http.post("/vaccine/wrongdep", sendData).then((result) => {
        $scope.wrongdep = result.data;
        console.log(result.data);
      });
    }
    function getvisitfail() {
      $http.post("/vaccine/visitfail", sendData).then((result) => {
        $scope.visitfail = result.data;
        console.log(result.data);
      });
    }

    patient();
    getwrongdep();
    getvisitfail();

    setInterval(function () {
      patient();
      getwrongdep();
      getvisitfail();
    }, 10000);
  });
};
