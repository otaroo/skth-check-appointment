export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/vaccine/patientperday", {
      templateUrl: "page/vaccine/patientperday.html",
      controller: "vaccinepatientperday",
      title: "vaccinepatientperday",
    });
  });
  app.controller("vaccinepatientperday", function ($scope, $http, $resource) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    $scope.sumAll = 0;
    $scope.sumAllOK = 0;
    $scope.patientAll = 0;
    $scope.vacc1 = 0;
    $scope.vacc2 = 0;
    $scope.sumAllVisit = 0;
    $scope.Astra1 = 0;
    $scope.Astra2 = 0;
    $scope.Astra = 0;
    $scope.Sinovac1 = 0;
    $scope.Sinovac2 = 0;
    $scope.Sinovac = 0;
    let sendData = {
      // Date: moment().format("2021-05-21"), // ทดสอบ
      Date: moment().format("YYYY-MM-DD"),
    };

    function patient() {
      $http.post("/vaccine/countvisit", sendData).then((result) => {
        $scope.sumAllVisit =  parseInt(result.data[0].count);
        if ($scope.sumAllVisit % 100 === 0 && $scope.sumAllVisit != 0) {
          startConfetti();
          setInterval(function () {
            stopConfetti();
          }, 30000);
        }
      });
      $http.post("/vaccine/countcc", sendData).then((result) => {
        $scope.sumCC = result.data[0].count;
      });
      $http.post("/vaccine/patientperday", sendData).then((result) => {
        $scope.sumAll = result.data.length;

        $scope.vacc1 = result.data.filter(
          (w) => w.vaccine_plan_no === 1
        ).length;
        $scope.vacc2 = result.data.filter(
          (w) => w.vaccine_plan_no === 2
        ).length;
        $scope.sumAllOK = result.data.filter(
          (w) => w.moph_update_result
        ).length;
      });
      $http.post("/vaccine/countpatient").then((result) => {
        $scope.patientAll1 = result.data[0].count;
        $scope.patientAll2 = result.data[1].count;
      });

      $http.post("/vaccine/patientvaccinetype", sendData).then((result) => {
        $scope.Astra1 = result.data[0].astra1;
        $scope.Astra2 = result.data[0].astra2;
        $scope.Astra = result.data[0].astra2;
        $scope.Sinovac1 = result.data[0].sinovac1;
        $scope.Sinovac2 = result.data[0].sinovac2;
        $scope.Sinovac = result.data[0].sinovac2;
      });
    }
    patient();
    setInterval(function () {
      patient();
    }, 10000);
  });

  app.directive("fadein", [
    "$animate",
    function ($animate) {
      return {
        scope: {
          val: "=",
        },
        link: function (scope, element, attrs) {
          scope.$watch("val", function (newValue, oldValue) {
            if (newValue === oldValue) return;
            $animate.addClass(element, "animate__fadeIn").then(function () {
              element.removeClass("animate__fadeIn");
            });
          });
        },
      };
    },
  ]);
};
