export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/vaccine/vaccinebybrand", {
      templateUrl: "page/vaccine/brand.html",
      controller: "vaccinebybrand",
      title: "vaccinebybrand",
    });
  });

  app.controller("vaccinebybrand", function ($scope, $http, $resource) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;

    $http.post("/vaccine/vaccinebybrand").then((result) => {
      $scope.brand = result.data[0];
      console.log(result);
    });
    $http.post("/vaccine/vaccinesummary").then((result) => {
      $scope.summary = result.data[0];
      console.log(result);
    });

    $http.post("/vaccine/vaccineperday").then((result) => {
      let data = result.data;
      console.log(result.data);

      let vaccine = {
        labels: data.map((data) => moment(data.date).format("DD/MM/YY") ),
        datasets: [
          {
            label: "จำนวนผู้ได้รับวัคซีน",
            backgroundColor: "#4E73DF",
            borderColor: "#4E73DF",
            fill: false,
            data: data.map((data) => data.vaccine),
            datalabels: {
              align: "top",
              anchor: "end",
            },
          },
          {
            label: "เข็ม 1",
            backgroundColor: "#FF0046",
            borderColor: "#FF0046",
            fill: false,
            data: data.map((data) => data.plan1),
            datalabels: {
              display: 'auto',
              align: "top",
              anchor: "end",
            },
          },
          {
            label: "เข็ม 2",
            backgroundColor: "#3AFF00",
            borderColor: "#3AFF00",
            fill: false,
            data: data.map((data) => data.plan2),
            datalabels: {
              display: 'auto',
              align: "top",
              anchor: "end",
            },
          },
        ],
      };
      lineChart("vaccineperday", vaccine);

    });
  });
};
