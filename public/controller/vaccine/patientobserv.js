export default () => {
    app.config(function($routeProvider) {
        $routeProvider.when("/vaccine/patientobserv", {
            templateUrl: "page/vaccine/patientobserv.html",
            controller: "patientobserv",
            title: "patientobserv",
        });
    });
    app.controller("patientobserv", function($scope, $http, $rootScope) {
        $rootScope.timeFormat = "HH:mm";
        let Today = moment().format("YYYY-MM-DD");
        $scope.page = 1;
        $scope.limit = 5;
        $scope.loop = 0;
        $scope.showloop = 0;
        $scope.showpage = 0;
        let loop = 1;

        function patientTotal() {
            let sendData = {
                Date: Today,
            };
            $http
                .post("/vaccine/patientobservtotal", sendData)
                .then((result) => {
                    let count = result.data[0].count;
                    console.log(result);
                    loop = Math.ceil(count / $scope.limit);
                    $scope.showloop = loop;
                })
                .catch((e) => console.error(e));
        }

        function patient() {
            $scope.showpage = $scope.page;
            $scope.time = moment().subtract(30, "minute").format("HH:mm");
            let sendData = {
                Date: Today,
                Page: $scope.page,
            };
            $http
                .post("/vaccine/patientobserv", sendData)
                .then((result) => {
                    $scope.patient = result.data;
                    if ($scope.page < loop) {
                        $scope.page += 1;
                    } else if (result.data.length <= $scope.limit) {
                        $scope.page = 1;
                        patientTotal();
                    }
                })
                .catch((e) => console.error(e));
        }
        patientTotal();
        patient();
        setInterval(function() {
            patient();
        }, 10000);
    });
};