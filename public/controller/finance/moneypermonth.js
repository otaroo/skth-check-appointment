export default () => {
  app.config(function ($routeProvider) {
    $routeProvider.when("/finance/moneypermonth", {
      templateUrl: "page/finance/moneypermonth.html",
      controller: "moneypermonth",
      title: "moneypermonth",
    });
  });

  app.controller("moneypermonth", function ($scope, $http, $resource) {
    moment.locale("th");
    let year = parseInt(moment().format("YYYY")) + 543;
    $scope.dateToday = moment().format("Do MMMM") + " " + year;
    $scope.timeToday = moment().format("HH:mm");

    Swal.fire({
      title: "กำลังทำงาน",
      allowEscapeKey: false,
      allowOutsideClick: false,
      onOpen: () => {
        Swal.showLoading();
      },
    });
    // let std = moment().subtract(12, "month");
    let sendData = {
      startDate:moment().subtract(12, "month").format("YYYY-MM-DD"),
      endDate: moment().format("YYYY-MM-DD"),
    };
    $http
      .post("/finance/moneypermonth", sendData)
      .then((res) => {
        Swal.close();
        Swal.fire({
          title: "สำเร็จ",
          text: res.data.mseeage,
          type: "success",
          timer: 2000,
        });

        let allward = {
          labels: res.data.map((data) => data.date),
          datasets: [
            {
              label: "ยอดเงิน (ล้านบาท)",
              backgroundColor: "#4E73DF",
              borderColor: "#4E73DF",
              fill: false,
              data: res.data.map((data) =>
                (parseFloat(data.sum) / 1000000).toFixed(2)
              ),

              datalabels: {
                align: "top",
                anchor: "end",
              },
            },
          ],
        };
        lineChart("moneypermonth", allward);
      })
      .catch((err) => console.log(err));
  });
};
