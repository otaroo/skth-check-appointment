export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/finance/moneyperday", { templateUrl: "page/finance/moneyperday.html", controller: "moneyperday", title: "moneyperday" })
    });

    app.controller('moneyperday', function ($scope, $http, $resource) {
        moment.locale('th');
        let year = parseInt(moment().format('YYYY')) + 543;
        $scope.dateToday = moment().format('Do MMMM') + ' ' + year;
        $scope.timeToday = moment().format('HH:mm');

        Swal.fire({
            title: 'กำลังทำงาน',
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                Swal.showLoading();
            }
        })

        let sendData = {
            startDate:  moment().subtract(1, 'month').format("YYYY-MM-DD"),
            endDate:  moment().format("YYYY-MM-DD"),
          };
        $http.post('/finance/moneyperday',sendData).then((res) => {
            Swal.close()
            Swal.fire({
                title: 'สำเร็จ',
                text: res.data.mseeage,
                type: 'success',
                timer: 2000
            })

            let allward = {
                labels: res.data.map(data =>  moment(data.date).format("DD/MM/YYYY")),
                datasets: [{
                    label: 'ยอดเงิน (แสนบาท)',
                    backgroundColor: "#4E73DF",
                    borderColor: "#4E73DF",
                    fill: false,
                    data: res.data.map(data => (parseFloat(data.sum)/100000).toFixed(2)),

                    datalabels: {
                        align: 'top',
                        anchor: 'end'
                    }
                }]
            };
            lineChart('moneyperday', allward);

        }).catch(err => console.log(err))
    })
}