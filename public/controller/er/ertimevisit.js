export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/er/ervisit", { templateUrl: "page/er/ervisit.html", controller: "ervisit", title: "ervisit" })
    });
    app.controller('ervisit', function ($scope, $http, $resource, $q, $timeout) {
        let startDate;
        let endDate;
        moment.locale('th')
        let year = parseInt(moment().format('YYYY')) + 543;
        // $scope.startDate = moment().format('Do MMMM') + ' ' + year;
        // $scope.endDate = moment().format('Do MMMM') + ' ' + year;
        // $scope.timeToday = moment().format('HH:mm');

        let dateOption = moment().format('Do MMMM') + ' ' + year
        let dateValue = moment().format('YYYY-MM-DD')
        $scope.dateOption = [{
            value: dateValue,
            name: dateOption,
            selected: true
        }]

        $scope.itemSelect = dateOption
        for (let index = 1; index <= 7; index++) {
            dateOption = moment().subtract(index, 'days').format('Do MMMM') + ' ' + year
            dateValue = moment().subtract(index, 'days').format('YYYY-MM-DD')
            $scope.dateOption.push({
                value: dateValue,
                name: dateOption,
                selected: false
            })
        }

        let ervisitChart
        initChart($scope.itemSelect);

        $scope.selectStartDate = function (date) {
            $("#endDate").datepicker('update', '');
            $scope.endDate = '';
            if (!date) return;
            var months_th = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",];
            let dateArr = date.split(" ");
            let month = months_th.findIndex(f => f == dateArr[1]) + 1
            startDate = `${(parseInt(dateArr[2]) - 543)}-${month.toString().padStart(2, '0')}-${dateArr[0]}`
            $("#endDate").datepicker("setStartDate",new Date(startDate));

        }
        $scope.selectEndDate = function (date) {
            if (!date) return;
            var months_th = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",];
            let dateArr = date.split(" ");
            let month = months_th.findIndex(f => f == dateArr[1]) + 1
            endDate = `${(parseInt(dateArr[2]) - 543)}-${month.toString().padStart(2, '0')}-${dateArr[0]}`
            getChartData();
        }

        function getChartData() {
            Swal.fire({
                title: 'กำลังทำงาน',
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            })
            let sendData = {
                startDate: startDate,
                endDate : endDate
            }
            let ervisit = $http.post('/er/ertimevisit', sendData)
            $q.all([ervisit]).then(result => {
                Swal.close()
                Swal.fire({
                    title: 'สำเร็จ',
                    type: 'success',
                    timer: 2000
                })
                let convertResult = result[0].data.map((d) => { 
                    return Object.entries(d); 
                })
                console.log(convertResult);
                ervisitChart.data.datasets[0].data = convertResult[0].map((data) => data[1] ).slice(1);
                ervisitChart.data.datasets[1].data = convertResult[1].map((data) => data[1] ).slice(1);
                // ervisitChart.data.labels = ['00:01 - 08:29','00:01 - 08:29','00:01 - 08:29','00:01 - 08:29','00:01 - 08:29','00:01 - 08:29']
                ervisitChart.update();
            })
        }

        function initChart() {
            let ervisit = {
                labels: ['00:01-08:29','08:30-11:59','12:00-12:59','13:00-16:29','16:30-20:29','20:30-23:59'],
                datasets: [
                    {
                        type: 'line',
                        label: 'วันหยุด',
                        borderColor: "#C0392B",
                        backgroundColor: "#C0392B",
                        fill: false,
                        datalabels: {
                            display: true,
                            align: 'top',
                            anchor: 'end'
                        }
                    }, 
                    {
                        type: 'line',
                        label: 'วันทำการ',
                        borderColor: "#9B59B6",
                        backgroundColor: "#9B59B6",
                        fill: false,
                        datalabels: {
                            display: true,
                            align: 'top',
                            anchor: 'end'
                        }
                    }  
                
                ]
            };
            let ctx = document.getElementById('ervisit').getContext('2d');
            ervisitChart = new Chart(ctx, {
                type: 'bar',
                data: ervisit,
                options: {
                    responsive: true,
                    plugins: {
                        datalabels: {

                            color: 'black',
                            display: function (context) {
                                return context.dataset.data[context.dataIndex] > 0;
                            },
                            font: {
                                weight: 'bold'
                            },
                            FontSize: '20',
                            title: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 10
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 20
                            }
                        }]
                    },
                    legend: {
                        display: true,
                        labels: {
                            fontSize: 16
                        },
                        align: 'start'
                    }
                }
            });
        }
    });
}