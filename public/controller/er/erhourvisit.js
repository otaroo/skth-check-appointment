export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/er/hourvisit", { templateUrl: "page/er/erhoutvisit.html", controller: "erhourvisit", title: "erhourvisit" })
    });
    app.controller('erhourvisit', function ($scope, $http, $resource, $q, $timeout) {
        
        moment.locale('th')
        let year = parseInt(moment().format('YYYY')) + 543;
        $scope.dateToday = moment().format('Do MMMM') + ' ' + year;
        $scope.timeToday = moment().format('HH:mm');

        let dateOption = moment().format('Do MMMM') + ' ' + year
        let dateValue = moment().format('YYYY-MM-DD')
        $scope.dateOption = [{
            value: dateValue,
            name: dateOption,
            selected: true
        }]

        $scope.itemSelect = dateOption
        for (let index = 1; index <= 7; index++) {
            dateOption = moment().subtract(index, 'days').format('Do MMMM') + ' ' + year
            dateValue = moment().subtract(index, 'days').format('YYYY-MM-DD')
            $scope.dateOption.push({
                value: dateValue,
                name: dateOption,
                selected: false
            })
        }

        let hourvisitChart
        initChart($scope.itemSelect);

        $scope.selectDate = function (date) {
            Swal.fire({
                title: 'กำลังทำงาน',
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            })
            var months_th = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",];
            let dateArr = date.split(" ");
            let month = months_th.findIndex(f => f == dateArr[1]) + 1
            let dateConvet = `${(parseInt(dateArr[2]) - 543)}-${month}-${dateArr[0]}`
            let sendData = {
                date: dateConvet
            }
            let hourvisit = $http.post('/er/erhourvisit', sendData)
            $q.all([hourvisit]).then(result => {
                Swal.close()
                Swal.fire({
                    title: 'สำเร็จ',
                    type: 'success',
                    timer: 2000
                })

                $scope.visit = result[0].data.reduce((sum, number) => {
                    return sum + parseInt(number.vn)
                }, 0)

                hourvisitChart.data.datasets[0].data = result[0].data.map(data => data.doctor)
                hourvisitChart.data.datasets[1].data = result[0].data.map(data => data.admit)
                hourvisitChart.data.datasets[2].data = result[0].data.map(data => data.dispense)
                hourvisitChart.data.datasets[3].data = result[0].data.map(data => data.rcpt)
                hourvisitChart.data.datasets[4].data = result[0].data.map(data => data.vn)
                hourvisitChart.data.labels = result[0].data.map(data => data.hous.substring(0, 5))
                hourvisitChart.update();
            })
        }
        $timeout(function () {
            $scope.selectDate($scope.itemSelect);
        }, 1000);


        function initChart() {
            let hourvisit = {
                datasets: [
                    {
                        type: 'line',
                        label: 'Doctor',
                        borderColor: "rgba(25,48,255,0.7)",
                        backgroundColor: "rgba(25,48,255,0.7)",
                        fill: false,
                        datalabels: {
                            display: function (context) {
                                var index = context.dataIndex;
                                if (context.dataset.data[index] <= 0) {
                                    return false
                                } else if (context.dataset.data[index] > 0 && context.dataset.data[index] < 30) {
                                    return 'auto'
                                }
                                if (context.dataset.data[index] >= 30) {
                                    return true
                                }
                            },

                            align: 'top',
                            anchor: 'start'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Admit',
                        borderColor: "#FF191F",
                        backgroundColor: "#FF191F",
                        fill: false,
                        datalabels: {
                            display: false,
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Dispense',
                        borderColor: "rgba(51,51,1,0.7)",
                        backgroundColor: "rgba(51,51,1,0.7)",
                        fill: false,
                        datalabels: {
                            display: function (context) {
                                var index = context.dataIndex;
                                if (context.dataset.data[index] <= 0) {
                                    return false
                                } else if (context.dataset.data[index] > 0 && context.dataset.data[index] < 30) {
                                    return 'auto'
                                }
                                if (context.dataset.data[index] >= 30) {
                                    return true
                                }
                            },
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Finance',
                        borderColor: "#839192",
                        backgroundColor: "#839192",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    {
                        type: 'bar',
                        label: 'Visit',
                        borderColor: "rgba(255,200,2,0.7)",
                        backgroundColor: "rgba(255,200,2,0.7)",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'end',
                            anchor: 'end'
                        }
                    }
                ]
            };
            // hourvisitChart = barChart('hourvisit', hourvisit);
            let ctx = document.getElementById('hourvisit').getContext('2d');

            hourvisitChart = new Chart(ctx, {
                type: 'bar',
                data: hourvisit,
                options: {
                    responsive: true,
                    plugins: {
                        datalabels: {

                            color: 'black',
                            display: function (context) {
                                return context.dataset.data[context.dataIndex] > 0;
                            },
                            font: {
                                weight: 'bold'
                            },
                            FontSize: '20',
                            title: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 10
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 20
                            }
                        }]
                    },
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16
                        },
                        align: 'start'
                    }
                }
            });
        }
    });
}