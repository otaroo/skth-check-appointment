export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/er/othourvisittype", { templateUrl: "page/er/erothoutvisittype.html", controller: "erothoutvisittype", title: "erothoutvisittype" })
    });
    app.controller('erothoutvisittype', function ($scope, $http, $resource, $q, $timeout) {

        moment.locale('th')
        let year = parseInt(moment().format('YYYY')) + 543;
        $scope.dateToday = moment().format('Do MMMM') + ' ' + year;
        $scope.timeToday = moment().format('HH:mm');

        let dateOption = moment().format('Do MMMM') + ' ' + year
        let dateValue = moment().format('YYYY-MM-DD')
        $scope.dateOption = [{
            value: dateValue,
            name: dateOption,
            selected: true
        }]

        $scope.itemSelect = dateOption
        for (let index = 1; index <= 7; index++) {
            dateOption = moment().subtract(index, 'days').format('Do MMMM') + ' ' + year
            dateValue = moment().subtract(index, 'days').format('YYYY-MM-DD')
            $scope.dateOption.push({
                value: dateValue,
                name: dateOption,
                selected: false
            })
        }

        let hourvisitChart
        initChart($scope.itemSelect);

        $scope.selectDate = function (date) {
            Swal.fire({
                title: 'กำลังทำงาน',
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            })
            var months_th = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",];
            let dateArr = date.split(" ");
            let month = months_th.findIndex(f => f == dateArr[1]) + 1
            let dateConvet = `${(parseInt(dateArr[2]) - 543)}-${month}-${dateArr[0]}`
            let sendData = {
                date: dateConvet
            }
            let hourvisit = $http.post('/er/erothourvisittype', sendData)
            $q.all([hourvisit]).then(result => {
                Swal.close()
                Swal.fire({
                    title: 'สำเร็จ',
                    type: 'success',
                    timer: 2000
                })

                $scope.visit = result[0].data.reduce((sum, number) => {
                    return sum + parseInt(number.vn)
                }, 0)
                console.log(result[0].data);

                hourvisitChart.data.datasets[0].data = result[0].data.map(data => data.type_1)
                hourvisitChart.data.datasets[1].data = result[0].data.map(data => data.type_2)
                hourvisitChart.data.datasets[2].data = result[0].data.map(data => data.type_3)
                hourvisitChart.data.datasets[3].data = result[0].data.map(data => data.type_4)
                hourvisitChart.data.datasets[4].data = result[0].data.map(data => data.type_5)
                hourvisitChart.data.labels = result[0].data.map(data => data.hous.substring(0, 5))
                hourvisitChart.update();
            })
        }
        $timeout(function () {
            $scope.selectDate($scope.itemSelect);
        }, 1000);


        function initChart() {
            let hourvisit = {
                datasets: [
                    {
                        type: 'line',
                        label: 'Resuscitation',
                        borderColor: "#F7D24B",
                        backgroundColor: "#F7D24B",
                        fill: false,
                        datalabels: {
                            display:'auto',
                            align: 'top',
                            anchor: 'start'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Emergency',
                        borderColor: "#2B40FC",
                        backgroundColor: "#2B40FC",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Urgent',
                        borderColor: "#FF191F",
                        backgroundColor: "#FF191F",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Semi Urgent',
                        borderColor: "#839192",
                        backgroundColor: "#839192",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'top',
                            anchor: 'end'
                        }
                    },
                    {
                        type: 'line',
                        label: 'Non Urgent',
                        borderColor: "#44420E",
                        backgroundColor: "#44420E",
                        fill: false,
                        datalabels: {
                            display: 'auto',
                            align: 'end',
                            anchor: 'end'
                        }
                    }
                ]
            };
            // hourvisitChart = barChart('hourvisit', hourvisit);
            let ctx = document.getElementById('hourvisit').getContext('2d');

            hourvisitChart = new Chart(ctx, {
                type: 'line',
                data: hourvisit,
                options: {
                    responsive: true,
                    plugins: {
                        datalabels: {

                            color: 'black',
                            display: function (context) {
                                return context.dataset.data[context.dataIndex] > 0;
                            },
                            font: {
                                weight: 'bold'
                            },
                            FontSize: '20', 
                            title: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 10
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontSize: 16,
                                padding: 20
                            }
                        }]
                    },
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16
                        },
                        align: 'start'
                    }
                }
            });
        }
    });
}