export default () => {
    app.config(function($routeProvider) {
        $routeProvider.when("/ipt/allward", { templateUrl: "page/ipt/allward.html", controller: "allward", title: "allward" })
    });
    app.controller('allward', function($scope, $http, $resource) {
        moment.locale('th');
        let year = parseInt(moment().format('YYYY')) + 543;
        $scope.dateToday = moment().format('Do MMMM') + ' ' + year;
        $scope.timeToday = moment().format('HH:mm');

        $scope.dtOptions = {
            paginationType: 'full_numbers',
            paging: false,
            displayLength: 100,
            lengthChange: false,
            searching: false,
            order: [
                [2, "desc"]
            ],
            language: {
                info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
                paginate: {
                    previous: "ก่อนหน้า",
                    next: "ถัดไป"
                },
                search: 'ค้นหา'
            },
            // dom: 'Bfrtip',
            // buttons: [{
            //     extend: 'excel',
            //     text: 'excel',
            //     title: '',
            //     filename: 'ผู้ป่วยใน',
            //     className: 'btn-primary'
            // }]
        }
        $scope.dtOptionsType = {
            paginationType: 'full_numbers',
            paging: false,
            displayLength: 100,
            lengthChange: false,
            searching: false,
            order: [
                [0, "desc"]
            ],
            language: {
                info: "ลำดับ _START_ ถึง _END_ ทั้งหมด _TOTAL_ รายการ",
                paginate: {
                    previous: "ก่อนหน้า",
                    next: "ถัดไป"
                },
                search: 'ค้นหา'
            },
            // dom: 'Bfrtip',
            // buttons: [{
            //     extend: 'excel',
            //     text: 'excel',
            //     title: '',
            //     filename: 'ผู้ป่วยใน',
            //     className: 'btn-primary'
            // }]
        }
        Swal.fire({
            title: 'กำลังทำงาน',
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                Swal.showLoading();
            }
        })

        $http.get('/ipt/allward').then((res) => {
            Swal.close()
            Swal.fire({
                title: 'สำเร็จ',
                text: res.data.mseeage,
                type: 'success',
                timer: 2000
            })
            $scope.ipt = res.data

            res.data.sort((a, b) => (parseFloat(a.percent) < parseFloat(b.percent)) ? 1 : -1)
            let allward = {
                labels: res.data.map(data => data.ward),
                datasets: [{
                    label: 'Active bed',
                    backgroundColor: "#4E73DF",
                    data: res.data.map(data => data.percent),

                    datalabels: {
                        align: 'top',
                        anchor: 'end'
                    }
                }]
            };
            barChart('allward', allward);


            // $scope.$apply()
            // $resource($scope.ipt).$promise;
        }).catch(err => console.log(err))

        $http.get('/ipt/ipttypebyward').then((res) => {
            console.log(res.data);
            $scope.ipttype = res.data
        }).catch(err => console.log(err))
    })
}