export default () => {
    app.config(function ($routeProvider) {
        $routeProvider.when("/ipt/activebed", { templateUrl: "page/ipt/activebed.html", controller: "activebed", title: "activebed" })
    });

    app.controller('activebed', function ($scope, $http, $resource) {
        moment.locale('th');
        let year = parseInt(moment().format('YYYY')) + 543;
        $scope.dateToday = moment().format('Do MMMM') + ' ' + year;
        $scope.timeToday = moment().format('HH:mm');

        Swal.fire({
            title: 'กำลังทำงาน',
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                Swal.showLoading();
            }
        })

        $http.get('/ipt/allActiveBed').then((res) => {
            Swal.close()
            Swal.fire({
                title: 'สำเร็จ',
                text: res.data.mseeage,
                type: 'success',
                timer: 2000
            })

            let allward = {
                labels: res.data.map(data => data.date),
                datasets: [{
                    label: 'อัตราครองเตียง',
                    backgroundColor: "#4E73DF",
                    borderColor: "#4E73DF",
                    fill: false,
                    data: res.data.map(data => data.activeBed),

                    datalabels: {
                        align: 'top',
                        anchor: 'end'
                    }
                }]
            };
            lineChart('activebed', allward);

        }).catch(err => console.log(err))
    })
}