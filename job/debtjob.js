const account = require('../query/account');
const moment = require('moment');

async function debtAccount() {
  let datetoday = moment().format('YYYY-MM-DD');
  let row = await account.getDebtPerDay(datetoday, undefined);
  row.forEach(item => {
    item.amount = item.sum;
    item.vstdate = moment(item.date).format('YYYY-MM-DD');
    item.create_date = moment().format('YYYY-MM-DD');
    item.create_user = '30007'
  });
  let pre = row.map(i => {
    return {
      vn: i.vn,
      hn: i.hn,
      pname: i.pname,
      fname: i.fname,
      lname: i.lname,
      vstdate: i.vstdate,
      amount: i.amount,
      vsttime: i.vsttime,
      invno: i.invno,
      pttype: i.pttype,
      create_date: i.create_date,
      create_user: i.create_user,
    }
  })
  console.log(pre[0]);
  await account.insertDebtPerDay(pre);
  console.log('บันทึกสำเร็จ'); 
}

var _export = {
  debtAccount: debtAccount
}

module.exports = _export;