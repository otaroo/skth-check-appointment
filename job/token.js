const axios = require("axios");
const https = require("https");
const crypto = require("crypto");

const secret = "$jwt@moph#";
const hash = crypto
  .createHmac("sha256", secret)
  .update("25182521Qwe@")
  .digest("hex");

const user = "bomb";
const password = hash.toUpperCase();

const agent = new https.Agent({
  rejectUnauthorized: false,
});

async function getToken() {
  return new Promise(async (resolve, reject) => {
    try {
      let row = await axios
        .get(
          `https://cvp1.moph.go.th/token?Action=get_moph_access_token&user=${user}&password_hash=${password}&hospital_code=10724`,
          { httpsAgent: agent }
        )
        .catch((e) => {
          let { error } = e.toJSON();
          throw error
        });
      resolve(row.data);
    } catch (error) {
      console.log(error);
      reject(error)
    }
  });
}

var _export = {
  getToken,
};

module.exports = _export;
