const cron = require('node-cron');
const debtjob = require('./debtjob.js');

function setjob() {

  console.log('Job schedule running');
  cron.schedule('59 23 * * *', function(){
    debtjob.debtAccount();
    console.log('running a task every minute');
});}

var _export = {
  setjob: setjob
}

module.exports = _export;