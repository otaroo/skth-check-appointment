const express = require("express");
const router = express.Router();
const recaptcha = require("../commonfn/recaptcha");
const mod11 = require("../commonfn/mod11");
const mophic = require("../commonfn/mophic");
const db = require("../query/register");
const moment = require("moment");

router.post("/checkmoph", async function (req, res) {
  if (!(req.body.token && req.body.cid)) return res.sendStatus(400);
  let tokenReCaptcha = req.body.token;
  let cid = req.body.cid;


  // Check Moph IC
  let moph = await mophic.ImmunizationTarget(cid).catch((e) => e);
  let {
    confirm_appointment_slot_count,
    person,
    queue_status,
    vaccine_history_count,
    confirm_appointment_slot
  } = moph;
  if (confirm_appointment_slot_count > 0 || vaccine_history_count > 0) {
    console.log("Moph IC found =>", cid);
    return res.send({
      message: 
      "กรณีมีนัดฉีดวัคซีนฯ กับโรงพยาบาลอื่นแล้ว หากท่านมีภูมิลำเนาอยู่ในเขตอำเภอเมือง"+
      "และมีความประสงค์ฉีดวัคซีนกับโรงพยาบาลสุโขทัย ขอให้ท่านติดต่อโรงพยาบาลปลายทาง"+
      "ที่มีคิวฉีดวัคซีนฯ เพื่อยกเลิกการจองดังกล่าวก่อน",
      vaccine: vaccine_history_count > 0 ? "<b>ท่านได้รับวัคซีนแล้ว</b>" : "",
      hos_name: confirm_appointment_slot_count > 0 ? "ท่านมีนัดฉีดวัคซีนกับ <br/>"+confirm_appointment_slot[0].hospital_name : "",
      status: false,
    });
  }

  // Check Register SKTH
  let regisPerson = await db.findRegister(cid);
  if (regisPerson.length > 0) {
    console.log("find Register found =>", cid);
    return res.send({
      message: "พบข้อมูลการลงทะเบียนในระบบแล้ว",
      status: false,
    });
  }

  return res.send({
    message: "สามารถลงทะเบียนได้",
    status: true,
  });

});



router.post("/", async function (req, res) {
  try {
    if (!(req.body.token && req.body.cid)) return res.sendStatus(400);
    let tokenReCaptcha = req.body.token;
    let cid = req.body.cid;

    // Check recaptcha
    let isReCap = await recaptcha(tokenReCaptcha).catch((e) => e);
    if (!isReCap)
      return res.send({ message: "กรุณาโหลดหน้าเว็บไซต์ใหม่", status: false });

    // Check Moph IC
    let moph = await mophic.ImmunizationTarget(cid).catch((e) => e);
    let {
      confirm_appointment_slot_count,
      person,
      queue_status,
      vaccine_history_count,
    } = moph;
    if (confirm_appointment_slot_count > 0 || vaccine_history_count > 0) {
      console.log("Moph IC found =>", cid);
      return res.send({
        message: "พบข้อมูลการได้รับวัคซีนหรือวันนัดฉีดวัคซีนกับโรงพยาบาลอื่นแล้ว",
        status: false,
      });
    }

    // Check Register SKTH
    let regisPerson = await db.findRegister(cid);
    if (regisPerson.length > 0) {
      console.log("find Register found =>", cid);
      return res.send({
        message: "พบข้อมูลการลงทะเบียนในระบบแล้ว",
        status: false,
      });
    }

    // Check Mod 11
    let checkMod11 = await mod11(cid);
    if (!checkMod11) {
      return res.send({
        message: "เลขบัตรประชาชนผิด",
        status: false,
      });
    }

    // Check Phone Number
    if (req.body.phone.length != 10 || !isFinite(parseInt(req.body.phone))) {
      console.log("Phone Number fail =>", req.body.phone);
      return res.send({
        message: "เบอร์โทรผิด ตัวเลข 10 หลักเท่านั้น",
        status: false,
      });
    }

    // Check Age
    const diffTime = Math.abs(moment() - moment(req.body.birthday));
    const diffYears = Math.ceil(diffTime / (1000 * 60 * 60 * 24 * 364));
    if (diffYears < 20) {
      console.log(
        "Age fail => ",
        req.body.birthday,
        " | ",
        diffYears + " Years"
      );
      return res.send({
        message: "อายุไม่ถึง 20 ปี",
        status: false,
      });
    }

    let regisData = {
      cid: cid,
      pname: req.body.pname,
      fname: req.body.fname,
      lname: req.body.lname,
      birthday: req.body.birthday,
      sex: req.body.sex,
      occupation: req.body.occupation,
      blood_grp: req.body.bloodtype,
      status_rlr: req.body.status,
      region: req.body.religion,
      addr: req.body.no,
      moopart: req.body.moo,
      chwpart: "64",
      tmbpart: req.body.tmbpart,
      amppart: "01",
      postcode: req.body.code,
      tell: req.body.phone,
      has_hn: "N",
      create_datetime: moment()
    };
    console.log(regisData);
    let insert = await db.addRegister(regisData);
    if (insert) {
      return res.send({ message: "ลงทะเบียนสำเร็จ" +
      "โรงพยาบาลสุโขทัยได้รับข้อมูลการลงทะเบียนของท่านเรียบร้อยแล้ว" +
      "เมื่อถึงกำหนดวันนัดฉีดวัคซีน โรงพยาบาลสุโขทัยจะส่งวันนัดหมายฉีดวัคซีนให้ท่านผ่านระบบ SMS ตามเบอร์โทรศัพท์ที่ท่านได้แจ้งไว้ในขั้นตอนการลงทะเบียน"
      , status: true });
    }
    res.send("ok");
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false });
  }
});
router.get("/occupation", async function (req, res) {
  let row = await db.getOccupation();
  res.send(row);
});

router.get("/religion", async function (req, res) {
  let row = await db.getReligion();
  res.send(row);
});

router.get("/chwpart", async function (req, res) {
  try {
    let row = await db.getChwpart();
    res.send(row);
  } catch (error) {
    console.log(error);
    res.send([]);
  }
});

router.get("/amppart", async function (req, res) {
  try {
    let row = await db.getAmppart("64");
    res.send(row);
  } catch (error) {
    console.log(error);
    res.send([]);
  }
});

router.get("/tmppart", async function (req, res) {
  try {
    let row = await db.getTmppart("64", "01");
    res.send(row);
  } catch (error) {
    console.log(error);
    res.send([]);
  }
});

router.get("/marrystatus", async function (req, res) {
  try {
    let row = await db.getMarrystatus();
    res.send(row);
  } catch (error) {
    console.log(error);
    res.send([]);
  }
});

module.exports = router;
