const express = require("express");
const router = express.Router();
const multer = require("multer");
const xlsx = require("node-xlsx").default;
const common = require("../query/common");
// upload file setup
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("layout");
  // res.render("close");
});
// router.get("/check", async function (req, res, next) {
//   let his = await common.check_db();
//   res.send("ok");
// });

// Post excel file
let fUpload = upload.single("file");
router.post("/upload", fUpload, function (req, res, next) {
  let buf_debtor = req.file;
  const workSheetsFromBuffer = xlsx.parse(buf_debtor.buffer);
  console.log(workSheetsFromBuffer[0].data);

  res.send("ok");
});

module.exports = router;
