const express = require("express");
const router = express.Router();
const axios = require("axios");
const https = require("https");
const db = require("../query/register");
const recaptcha = require("../commonfn/recaptcha");
const mophic = require("../commonfn/mophic");
const mod11 = require("../commonfn/mod11");
require("dotenv").config();

const agent = new https.Agent({
  rejectUnauthorized: false,
});

router.post("/checkapp", async function (req, res) {
  if (!req.body.token) return res.sendStatus(400);
  if (!req.body.cid) return res.sendStatus(400);
  let tokenReCaptcha = req.body.token;
  let cid = req.body.cid;

  let isOK = await recaptcha(tokenReCaptcha).catch((e) => e);
  if (!isOK)
    return res.send({
      message: "กรุณาโหลดหน้าเว็บไซต์ใหม่",
      recap: false,
      btnapp: false,
    });

  // Check Mod 11
  let checkMod11 = await mod11(cid);
  if (!checkMod11) {
    console.log("mod11 fail =>", cid);
    return res.send({
      message: "เลขบัตรประชาชนผิด",
      mod11: false,
      btnapp: false,
    });
  }

  // Check SKTH
  let regisPerson = await db.findRegister(cid);
  if (regisPerson.length > 0) {
    console.log("find Register found =>", cid);
    return res.send({
      message: "พบข้อมูลการลงทะเบียนในระบบโรงพยาบาลสุโขทัย",
      btnapp: false,
      sktdate: regisPerson[0].create_datetime,
    });
  }

  let moph = await mophic.ImmunizationTarget(cid).catch((e) => e);
  if (!moph) {
    console.log('error moph ic',moph);
    let data = {
      btnapp: false,
      message: "กรุณาลองใหม่",
    };
    res.send(data);
    return;
  }

  let { confirm_appointment_slot, person, queue_status } = moph;
  let { prefix, first_name, last_name, person_type_name } = person;

  if (
    person &&
    Object.keys(person).length === 0 &&
    person.constructor === Object
  ) {
    console.log('person fail =>', cid);
    let data = {
      btnapp: true,
      message: "ไม่พบข้อมูลการลงทะเบียนกับโรงพยาบาลสุโขทัย",
    };
    res.send(data);
    return;
  }

  if (
    confirm_appointment_slot.some((item) => {
      return "10724" != item.hospital_code;
    })
  ) {
    console.log('appointment_slot fail  =>', cid);
    let data = {
      btnapp: true,
      message: "ไม่พบข้อมูลการลงทะเบียนกับโรงพยาบาลสุโขทัย",
    };
    res.send(data);
    return;
  }

  let data = {
    confirm_appointment_slot,
    prefix,
    first_name,
    last_name,
    person_type_name,
    queue: queue_status,
    status: true,
    message: "สำเร็จ",
    btnapp: true,
  };
  console.log(data);
  res.send(data);
  return;
});

module.exports = router;
