const parser = require('fast-xml-parser');
const fs = require('fs');
const moment = require('moment');
const fn = require('../commonfn/commonfn')

let options = {
    attributeNamePrefix: "@_",
    attrNodeName: "attr", //default is 'false'
    textNodeName: "#text",
    ignoreAttributes: true,
    ignoreNameSpace: false,
    allowBooleanAttributes: false,
    parseNodeValue: true,
    parseAttributeValue: false,
    trimValues: true,
    cdataTagName: "__cdata", //default is 'false'
    cdataPositionChar: "\\c",
    localeRange: "", //To support non english character in tag/attribute values.
    parseTrueNumberOnly: false
};

function readItem(buf) {
    return new Promise(async (resolve, reject) => {
        let xmlData = buf.toString('utf8');
        let tbills = [];
        if (parser.validate(xmlData) === true) { //optional (it'll return an object in case it's not valid)
            let jsonObj = parser.parse(xmlData, options);
            tbills = jsonObj.STMSTM.TBills[0].TBill;
            let data = tbills.map(e => {
                let dttran = e.dttran.split('T');
                let invno =e.invno.toString().split(',')
                return {
                    hn: e.hn.toString().padStart(9, '0'),
                    invno: invno[0],
                    date: dttran[0],
                    time: dttran[1],
                    amount: e.amount,
                    rid: e.rid.toString(),
                    create_date: moment().format('YYYY-MM-DD'),
                    create_user: '30007'
                }
            })
            resolve(data);
        }
    });

}
function readMain(buf) {
    return new Promise(async (resolve, reject) => {
        let xmlData = buf.toString('utf8');
        let tbills = [];
        if (parser.validate(xmlData) === true) { //optional (it'll return an object in case it's not valid)
            let jsonObj = parser.parse(xmlData, options);
            tbills = jsonObj.STMSTM.STMdoc.split('_');
            let dateArr = jsonObj.STMSTM.datedue.split('  ');
            let year = parseInt(dateArr[2]) - 543;
            let month = fn.getMonth(dateArr[1]);
            let data = {
                statement_no: tbills[2],
                statement_amount: jsonObj.STMSTM.STMdat[0].Dat.Tamount,
                statement_datetime: `${year}-${month.toString().padStart(2, '0')}-${dateArr[0]}`,
                statement_count: jsonObj.STMSTM.STMdat[0].Dat.Tcount,
                create_date: moment().format('YYYY-MM-DD'),
                create_user: '30007',
                statement_type: jsonObj.STMSTM.stmAccountID
            };           
            resolve(data);
        }
    });

}
var xmlModel = {
    readItem: readItem,
    readMain: readMain
}

module.exports = xmlModel;